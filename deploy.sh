#!/usr/bin/env bash

#Variables after dos2unix
NOW=$(date +"%d-%m-%Y %T")
GITADDRESS="git@gitlab.com:devmr/reactbuilt_covid19mg.git"

# "build_vcs" directory does not exist
if [[ ! -d "build_vcs/" ]]
then

#  Create directory and go to directory
  mkdir build_vcs/
  cd build_vcs/
#  init git in directory
  git init
  git remote add origin ${GITADDRESS}
#  change directory parent
  cd ../
fi

# Go to directory; pull and push
cd build_vcs/
git pull origin master
rm -rf *
cp -r ../build/* .
git add .
git commit -m "Deploy ${NOW}"
git push -u origin master
