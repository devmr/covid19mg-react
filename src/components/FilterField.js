import React, {useState} from 'react';
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Switch from "@material-ui/core/Switch/Switch";
import Grid from "@material-ui/core/Grid";
import {Select} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField/TextField";
import FormControl from "@material-ui/core/FormControl";
import {Formatter} from "../utils/formatter";
import DateField from "./DateField";
import Box from "@material-ui/core/Box";
import {Field} from "formik";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import {Config} from "../utils/config";
import DistrictService from "../service/DistrictService";
import AutocompleteField from "./AutocompleteField";

const Filterfield = (props) => {


    let rootField = props.field;
    let t = props.t;
    let filter = props.filter;

    const [fieldValue, setFieldValue] = useState([]);

    // Option filter range field
    const optionFilterRange = Config.getFilterOptionsRange();

    // Option filter Date field
    const optionFilterDate = Config.getFilterOptionsDate();

    const handleChange = (e) => {
        console.log(e)
        console.log(JSON.stringify(fieldValue))
    };

    let filterField = [];
    filter.map( (row) => {
        filterField[row.key] = {rows: row.rows, totalItems: row.totalItems};
    })

    switch (rootField.filter.type) {
        case 'switch':
            return (
                <React.Fragment>
                    <FormControlLabel
                        control={<Switch />}
                        label={t(rootField.label)}
                        name={`switch_${rootField.key}`}
                    />
                </React.Fragment>
            )
            break;

        case 'text':
            return (
                <React.Fragment>
                    {/*<pre>fieldValue: {JSON.stringify(fieldValue)}</pre>*/}



                    <Field name={`text_${rootField.key}`}>
                        {({ field, form, meta }) => (
                    <TextField
                        type="text"
                        margin="normal"
                        fullWidth
                        {...field}
                        label={t(rootField.label)}
                    />)}
                    </Field>

                   </React.Fragment>
            )
            break;
        case 'translation':
            return (
                <Field name={`translation_${rootField.key}`}>
                    {({ field, form, meta }) => (

                        <FormControl
                            margin="normal"
                            fullWidth
                            variant="outlined"

                        >
                            <InputLabel>{t(rootField.label)}</InputLabel>
                            <Select
                                {...field}
                                margin="normal"
                            >
                                <MenuItem value="">{t('field.select_none')}</MenuItem>
                                {
                                    typeof filterField[rootField.key] !== 'undefined' ?
                                    filterField[rootField.key]['rows'].map((row) => {
                                            return (
                                                <MenuItem
                                                    value={row.id}>{Formatter.format(row, {options: rootField.filter.options})}</MenuItem>
                                            )
                                        })
                                        : ''
                                }
                            </Select>
                        </FormControl>


                    )}
                </Field>
            )
            break;
        case 'nativeEntity':
            return (
                <Field name={`nativeEntity_${rootField.key}`}>
                    {({ field, form, meta }) => (

                        <FormControl
                            margin="normal"
                            fullWidth
                            variant="outlined"

                        >
                            <InputLabel>{t(rootField.label)}</InputLabel>
                            <Select
                                {...field}
                                margin="normal"
                            >
                                <MenuItem value="">{t('field.select_none')}</MenuItem>
                                {

                                    typeof filterField[rootField.key] !== 'undefined' ?
                                        filterField[rootField.key]['rows'].map((row) => {
                                            return (
                                                <MenuItem
                                                    value={row.id}>{row.name}</MenuItem>
                                            )
                                        })
                                        : ''
                                }
                            </Select>
                        </FormControl>


                    )}
                </Field>
            )
            break;
        case 'relation':
            return (
                <Field name={`relation_${rootField.key}`}>
                    {({ field, form, meta }) => (

                        <FormControl
                            margin="normal"
                            fullWidth
                            variant="outlined"

                        >
                            <InputLabel>{t(rootField.label)}</InputLabel>
                            <Select
                                {...field}
                                margin="normal"
                            >
                                <MenuItem value="">{t('field.select_none')}</MenuItem>
                                {
                                    typeof filterField[rootField.key] !== 'undefined' ?
                                        filterField[rootField.key]['rows'].map((row) => {
                                            return (
                                                <MenuItem
                                                    value={row}>{row}</MenuItem>
                                            )
                                        })
                                        : ''
                                }
                            </Select>
                        </FormControl>


                    )}
                </Field>
            )
            break;
        case 'date':
            return (
                <React.Fragment>
                    <InputLabel>{t(rootField.label)}</InputLabel>
                <Grid container justify="space-around">

                    <Field name={`optiondate_${rootField.key}`}>
                        {({ field, form, meta }) => (
                            <Select
                                margin="normal"
                                {...field}
                            >
                            <MenuItem value="">{t('field.select_none')}</MenuItem>
                        {
                            Object.keys(optionFilterDate).map( (row) => {
                                return (
                                    <MenuItem value={row}>{ t(optionFilterDate[row]) }</MenuItem>
                                )
                            })
                        }
                    </Select>)}
                    </Field>
                    <DateField
                        format="dd-MM-yyyy"
                        variant="inline"
                        autoOk={true}
                        name={`date_${rootField.key}`}
                    />

                </Grid>


                </React.Fragment>
            )
            break;
        case 'range':
            return (
                <React.Fragment>
                    <br /><InputLabel>{t(rootField.label)}</InputLabel>
                <Grid container justify="space-around">
                    <Field name={`optionrange_${rootField.key}`}>
                        {({ field, form, meta }) => (
                            <Select
                                margin="normal"
                                {...field}
                            >
                                <MenuItem value="">{t('field.select_none')}</MenuItem>
                                {
                                    Object.keys(optionFilterRange).map( (row) => {
                                        return (
                                            <MenuItem value={row}>{ t(optionFilterRange[row]) }</MenuItem>
                                        )
                                    })
                                }
                            </Select>
                        )}
                    </Field>
                    <Field name={`range_${rootField.key}`}>
                        {({ field, form, meta }) => (
                            <TextField
                                {...field}
                                type="number"
                                min="0"
                                margin="normal"
                                label={t(rootField.label)}
                            />
                        )}
                    </Field>
                </Grid>
                </React.Fragment>
            )
            break;
    }
}



export default Filterfield;
