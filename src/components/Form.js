import React from 'react';
import Container from "@material-ui/core/Container";
import Alert from "@material-ui/lab/Alert/Alert";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField/TextField";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import DeleteIcon from '@material-ui/icons/Delete';
import _ from 'lodash';
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Dialog from "@material-ui/core/Dialog/Dialog";
import SessionService from "../service/SessionService";

import 'date-fns';

import {Field, Formik} from "formik";
import * as Yup from "yup";

import { withTranslation} from "react-i18next";

import {Config} from "../utils/config";
import Switch from "@material-ui/core/Switch/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import strings from "locutus/php/strings";
import {Formatter} from "../utils/formatter";
import Moment from "react-moment";
import Typography from "@material-ui/core/Typography";
import DateField from "./DateField";
import Handlebars from "handlebars"
import moment from "moment";
import DatesService from "../service/DatesService";
import DistrictService from "../service/DistrictService";
import AutocompleteField from "./AutocompleteField";

const fieldInsidePrimary = []
const fieldInsideSecondary = []

let valuesInitial = {}
let valuesImmutable = {}
let validatorFields = {}

class Form extends React.Component {
    constructor(props){
        super(props);
        this.state = {

            // Flash Messages / Snackbar messages
            // message: '',
            messageSeverity: '',
            snackmessage: '',
            messageDialogSearch: '',
            messageDialogSearchSeverity: '',

            // Edit mode - single or multiple
            id: typeof props.match !== 'undefined' ? props.match.params.id : 0,
            idMultiple: typeof props.match !== 'undefined' && (props.match.params.id.split(',').length>1 || props.match.params.id.split(',').includes('*')),

            // Edit multiple
            rowsMultiple: [],
            showAll: false, // Collapsible

            // Dialog confirm - edit multiple
            openDialog: false,
            valueForm: [], // State value formik

            fieldOptions: [],
            fieldValues: {},

            // Date creator field Name
            datefieldName: '',

            // Autocomplete
            filterAutocompleteValue: [],

        }

        // Fields state
        if (props.model.form.fields.length) {
            props.model.form.fields.map((row) => {
                let fieldName = row['key'];
                let fieldType = row.type

                valuesInitial[fieldName] = ''

                if (['relation', 'translation'].includes(fieldType)) {
                    this.state.fieldOptions.push({[fieldName]: []})
                }
            })
        }

        // console.log('Form.js:474: ' + JSON.stringify(this.state.fieldOptions[0]))

        this.flashMessage = this.flashMessage.bind(this);
        this.handleList = this.handleList.bind(this);

        this.onChange = this.onChange.bind(this);
    }

    prepareFieldsToSave = (values) => {

        console.log('Form.js:474: values: (before) ' + JSON.stringify(values))

        let _values = {}

        this.props.model.form.fields.map((row, i) => {

            let fieldName = typeof row['key'] !== 'undefined' ? row['key'] : ''
            let fieldType = typeof row['type'] !== 'undefined' ? row['type'] : ''
            let fieldValue = values[fieldName]
            _values[fieldName] = fieldValue
                let fieldAttributes = typeof row.attributes !== 'undefined' ? row.attributes : []
            let fieldJoin = typeof row.submit !== 'undefined' && typeof row.submit.joinfield !== 'undefined' ? row.submit.joinfield : ''
            let fieldValueAdded = typeof row.submit !== 'undefined' && typeof row.submit.valuefield !== 'undefined' ? row.submit.valuefield : ''
            let fieldpatternSearch = typeof row.submit !== 'undefined' && typeof row.submit.patternSearch !== 'undefined' ? row.submit.patternSearch : ''
            let fieldpatternReplace = typeof row.submit !== 'undefined' && typeof row.submit.patternReplace !== 'undefined' ? row.submit.patternReplace : ''

            let fieldSaveFormat = typeof row.submit !== 'undefined' && typeof row.submit.format !== 'undefined' ? row.submit.format : ''
            let fieldSavePlain = typeof row.submit !== 'undefined' && typeof row.submit.plain !== 'undefined' ? row.submit.plain : false

            let fieldOption = typeof row['option'] !== 'undefined' ? row['option'] : {}

            let fieldOptionKey  = typeof fieldOption['key'] !== 'undefined' ? fieldOption['key'] : ''


            if (!fieldSavePlain && fieldOptionKey !=='' && typeof fieldValue[fieldOptionKey]!=='undefined') {
                _values[fieldName] = fieldValue[fieldOptionKey]
            }

            if (fieldType=='autocomplete' && typeof fieldValue['value'] !== 'undefined' && fieldValue['value'] !== '') {
                _values[fieldName] = strings.sprintf(fieldSaveFormat, fieldValue['value'])
            }

            // convert using Sprintf like php - translation
            if (fieldSaveFormat !== '' && fieldValue !=='' && fieldSavePlain) {
                _values[fieldName] = strings.sprintf(fieldSaveFormat, fieldValue)
            }

            // number attribute
            if (fieldAttributes.includes('number') &&  fieldValue !=='') {
                _values[fieldName] = parseInt(fieldValue)
            }

            // Add suffix to value
            if (fieldValue !=='' && fieldJoin!=='' && fieldValueAdded!=='' && values[fieldJoin]) {
                _values[fieldName] = fieldValue + fieldValueAdded;
            }

            // replace value with regex
            if (fieldValue !=='' && fieldpatternSearch!=='' && fieldpatternReplace!=='') {
                _values[fieldName] = fieldValue.replace(fieldpatternSearch, fieldpatternReplace);
            }

        })

        console.log('Form.js:474: values: (after) ' + JSON.stringify(_values))

        return _values;

    }

    handleOpenDialog = () => {
        this.setState({ openDialog: true });
    }

    handleCloseDialog = () => {
        this.setState({ openDialog: false });
    }

    saveCloneMultiple = (values) => {


        let newValues = {};
        // rewrite values with filled values only
        for (let k in values) {
            if (!Config.isEmpty(values[k])) {
                newValues[k] = values[k];
            }
        }

        // Show progressbar
        this.setState({ isSubmitted: true });

        // Each rows inside multiple Array
        this.state.rowsMultiple.map( (row) => {

            let id = row['id'];

            // Clone each row
            this.props.model.service.duplicate(id).then(res => {

                // Cloned ID
                let id = res.data['id'];

                // Update each row
                this.props.model.service.update(newValues, id);

                // Hide progressbar
                this.setState({ isSubmitted: false });

                // Show Snackbar
                this.setState({snackmessage: this.props.t('text.success_duplicated_multiple')})

            });

            return true;
        });
    }

    handleOkDialog = () => {

        // Close dialog
        this.handleCloseDialog()

        // Get values of form
        let values = this.prepareFieldsToSave(this.state.valueForm);

        console.log('Form.js:474: values: (handleOkDialog) ' + JSON.stringify(values))

        let newValues = {};
        // rewrite values with filled values only
        for (let k in values) {
            if (!Config.isEmpty(values[k])) {
                newValues[k] = values[k];
            }
        }

        if (this.state.rowsMultiple.length===0) {

            // Show progressbar
            this.setState({ isSubmitted: true });

            // reset Snackbar
            this.setState({snackmessage: ''})

            // Update API Platform
            this.props.model.service.updateAll(newValues).then(res => {
                // Http Code 200 or 201
                if ([200, 201].includes(res.status)) {

                    // Show Snackbar
                    this.setState({snackmessage: this.props.t('text.success_updated_multiple')})

                    // Hide progressbar
                    this.setState({ isSubmitted: false });

                    setTimeout(() => {
                        this.handleList();
                    }, 500);

                }
            });

            return;
        }

        let rowCountUpdated = 0;
        // Each rows inside multiple Array
        this.state.rowsMultiple.map( (row) => {

            // Show progressbar
            this.setState({ isSubmitted: true });

            // reset Snackbar
            this.setState({snackmessage: ''})

            let id = row.id;

            // Update API Platform
            this.props.model.service.update(newValues, id).then(res => {

                // Http Code 200 or 201
                if ([200, 201].includes(res.status)) {

                    // Show Snackbar
                    this.setState({snackmessage: this.props.t('text.success_updated_multiple')})

                    // Hide progressbar
                    this.setState({ isSubmitted: false });

                    rowCountUpdated++;

                }

            }).catch((error) => {
                if (error.response) {
                    console.log(error.response.data.message)
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request and triggered an Error
                    console.log(error.message)
                }
            });



            return true;

        });

        setTimeout(() => {
            if (rowCountUpdated === this.state.rowsMultiple.length) {
                this.handleList();
            }
        }, 5000);

    }

    handleList = () => {
        let pathList = this.props.model.list.actions.pathlist
        // Go to all rows list
        this.props.history.push(SessionService.get('referrer') !== '' ? SessionService.get('referrer') : pathList)
    }

    getMultipleResult(result) {
        let rowsMultiple = typeof result['hydra:member'] !== 'undefined' ? result['hydra:member'] : [];

        // Set states
        this.setState({rowsMultiple: rowsMultiple});
        this.setState({ isSubmitted: false });

        console.log('Form.js 299 - rowsMultiple: ' + JSON.stringify(this.state.rowsMultiple))
    }

    getFieldByKey(key) {
        let field = {}
        if (this.props.model.form.fields.length) {
            this.props.model.form.fields.map((row) => {
                let fieldName = typeof row['key'] !== 'undefined' ? row['key'] : ''
                if (fieldName === key) {
                    field = row
                }
            })
        }
        return field
    }
    getResultByType(result, row) {

        let fieldName = typeof row['key'] !== 'undefined' ? row['key'] : ''
        let fieldType = typeof row['type'] !== 'undefined' ? row['type'] : ''
        let fieldOption = typeof row['option'] !== 'undefined' ? row['option'] : {}
        let fieldValueAdded = typeof row.submit !== 'undefined' && typeof row.submit.valuefield !== 'undefined' ? row.submit.valuefield : ''
        let fieldInversed = typeof row['inversedfield'] !== 'undefined' ? row['inversedfield'] : ''

        let resultValue = typeof result[fieldName] !== 'undefined' ? result[fieldName] : {};

        // nativeEntity or translation
        let fieldOptionKey  = typeof fieldOption['key'] !== 'undefined' ? fieldOption['key'] : ''
        let fieldOptionValue  = typeof fieldOption['value'] !== 'undefined' ? fieldOption['value'] : ''

        // switch
        if (fieldType === 'switch' && fieldName !== '' && fieldInversed!=='') {
            let rowInversed = this.getFieldByKey(fieldInversed)
            let valueFieldFromInversed = typeof rowInversed.submit !== 'undefined' && typeof rowInversed.submit.valuefield !== 'undefined' ? rowInversed.submit.valuefield : ''
            if (valueFieldFromInversed === fieldName) {
                return true;
            }
        }

        // text or relation
        if (!['nativeEntity', 'translation', 'autocomplete'].includes(fieldType)  && fieldName !== '') {
            resultValue = fieldValueAdded !== '' ? resultValue.replace(fieldValueAdded, '') : resultValue;
            return resultValue;
        }



        //Autocomplete
        if (['autocomplete'].includes(fieldType)  && fieldName !== '') {
            resultValue = {value: resultValue[fieldOptionKey], label: resultValue[fieldOptionValue]};
            return resultValue;
        }

        // datecreator
        if (['datecreator'].includes(fieldType)  && fieldName !== '') {
            resultValue =  {value: resultValue[fieldOptionKey], label: resultValue[fieldOptionValue][fieldOptionValue]};
            return resultValue;
        }

        // Otherwise
        if (fieldOptionKey !== '') {
            return resultValue[fieldOptionKey]
        }

        return ""

    }

    getSingleResult(result) {

        if (this.props.model.form.fields.length) {
            this.props.model.form.fields.map((row) => {
                let fieldName = typeof row['key'] !== 'undefined' ? row['key'] : ''
                valuesInitial[fieldName] = this.getResultByType(result, row)
                valuesImmutable[fieldName] = valuesInitial[fieldName]
            })

            this.setState({fieldValues : valuesInitial})

        }


        console.log('Form.js:474 - state.fieldValues (after): ' + JSON.stringify(this.state.fieldValues))

        // Hide progress
        this.setState({ isSubmitted: false });

    }

    findIndexFieldOption(fieldName) {

        let index = 0;

        if (fieldName !== null) {

            // field state array
            let stateFieldOtions = this.state.fieldOptions;

            // Change or add state
            stateFieldOtions.map((row, i) => {
                let currentField  = stateFieldOtions[i];
                for(let currentFieldName in row) {
                    if (fieldName == currentFieldName) {
                        index = i;
                    }
                }
            })


        }

        return index

    }

    addOrUpdateState(fieldName, fieldValue) {

        let added = false;

        if (fieldName !== null) {

            // field state array
            let stateFieldOtions = this.state.fieldOptions;

            // Keep list field name
            let listFieldname = [];
            stateFieldOtions.map((row, i) => {
                for(let obj in row) {
                    listFieldname.push(obj)
                }
            })

            // Change or add state
            stateFieldOtions.map((row, i) => {
                let currentField  = stateFieldOtions[i];
                for(let currentFieldName in row) {
                    if (listFieldname.includes(fieldName) && fieldName == currentFieldName) {
                        stateFieldOtions[i][fieldName] = fieldValue;
                    }

                    if (!listFieldname.includes(fieldName)) {
                        added = true;
                    }
                }
             })

            if (added) {
                stateFieldOtions.push({[fieldName]:fieldValue})
            }
        }
    }

    findLabelField(field) {
        let fieldLabel = ''
            this.props.model.form.fields.map((row, i) => {
            let fLabel = typeof row['label'] !== 'undefined' ? row['label'] : ''
            let fName = typeof row['key'] !== 'undefined' ? row['key'] : ''

            if (fName === field && fLabel !=='') {
                fieldLabel = this.props.t(fLabel)
            }

        })

        return fieldLabel
    }

    findTemplateMultipleField(field) {
        let template = ''
        this.props.model.form.fields.map((row, i) => {
            let fTemplate = typeof row['option'] !== 'undefined' && typeof row['option']['templateMultiple'] !== 'undefined' ? row['option']['templateMultiple'] : ''
            let fName = typeof row['key'] !== 'undefined' ? row['key'] : ''

            if (fName === field && fTemplate !=='') {
                template = fTemplate
            }

        })

        return template
    }

    findOptionField(field) {
        let option = []
        this.props.model.form.fields.map((row, i) => {
            let fName = typeof row['key'] !== 'undefined' ? row['key'] : ''
            let fieldOption = typeof row['option'] !== 'undefined' ? row['option'] : []

            if (fName === field) {
                option = fieldOption
            }

        })

        return option
    }

    findFilterField(field) {
        let option = []
        this.props.model.form.fields.map((row, i) => {
            let fName = typeof row['key'] !== 'undefined' ? row['key'] : ''
            let fieldFilter = typeof row['filter'] !== 'undefined' ? row['filter'] : []

            if (fName === field) {
                option = fieldFilter
            }

        })

        return option
    }


    findContentField(fieldInsidePrimary, row) {


        // Array unique
        fieldInsidePrimary = [...new Set(fieldInsidePrimary)]

        let listContent = [];

        fieldInsidePrimary.map((field) => {
            if (row[field] !== null) {
                let fieldLabel = this.findLabelField(field)
                let fieldTemplate = this.findTemplateMultipleField(field)
                let fieldFilter = this.findFilterField(field)



                let plainValue = Formatter.format(row[field],{options: fieldFilter['options']})
                let fieldValue = typeof plainValue!== 'undefined' && typeof plainValue['props'] !== 'undefined' ? plainValue['props']['children'] : plainValue;

                // console.log(JSON.stringify(fieldFilter['options']))

                // Handlebars
                const template = Handlebars.compile(fieldTemplate);
                let fieldContent = template({label: fieldLabel, value: fieldValue})
                listContent.push(fieldContent)
            }
        })

        let str = listContent.join(' - ');



        return str
    }

    componentDidMount() {

        let apiService = this.props.model.service;


        let fieldOptionsCount = 0

        // Fields state
        if (this.props.model.form.fields.length) {
            this.props.model.form.fields.map((row, i) => {

                let fieldName = row['key'];
                let fieldType = row.type
                let fieldValidatorList = typeof row['validator']!=='undefined' ? row['validator'] : [];
                let fieldServiceMethod = typeof row['serviceMethod'] !== 'undefined' ? row['serviceMethod'] : null;
                let fieldServiceClass = typeof row['serviceClass'] !== 'undefined' ? row['serviceClass'] : null;
                let fieldDefaultValue = typeof row['defaultValue'] !== 'undefined' ? row['defaultValue'] : ""
                let fieldPattern = typeof row['pattern'] !== 'undefined' ? row['pattern'] : ""

                let fieldOptions = typeof row.option !== 'undefined' ? row.option : []
                let fieldOptionsKey = typeof fieldOptions !== 'undefined' ? fieldOptions['key'] : ''
                let fieldOptionsValue = typeof fieldOptions !== 'undefined' ? fieldOptions['value'] : ''

                if (row['insideRowMultiplePrimary']!==null && row['insideRowMultiplePrimary']===true) {
                    fieldInsidePrimary.push(row['key'])
                }
                if (row['insideRowMultipleSecondary']!==null && row['insideRowMultipleSecondary']===true) {
                    fieldInsideSecondary.push(row['key'])
                }

                // Date creator
                if (fieldType === 'datecreator' && fieldDefaultValue === '') {
                    fieldDefaultValue = {}
                    fieldDefaultValue[fieldOptionsKey] = "";
                }

                valuesInitial[fieldName] = fieldDefaultValue

                // console.log('Form.js:474 - fieldServiceClass: ' + JSON.stringify(fieldServiceClass !== null) + ' - fieldServiceMethod: ' + fieldServiceMethod + ' - fieldName:' + fieldName + ' - fieldType: ' + fieldType + ' -- ' + JSON.stringify(fieldValidatorList))

                if (fieldValidatorList.includes('required')) {
                    validatorFields[fieldName] = Yup.string().required(this.props.t('error.field_empty'))
                }
                if (fieldValidatorList.includes('regex') && fieldValidatorList.includes('string') && fieldPattern !== "") {
                    validatorFields[fieldName] = Yup.string().matches(fieldPattern, this.props.t('error.field_incorrect'))
                }

                if (['relation', 'nativeEntity', 'translation' ].includes(fieldType) && fieldServiceClass !== null) {

                        // console.log('Form.js:474 ' + fieldName)
                        fieldServiceClass[fieldServiceMethod]().then(res => {

                            if(res.status === 200){

                                // console.log('Form.js:474 - res.data: ' + JSON.stringify(res.data))
                                // console.log('Form.js:474 - state.fieldOptions (before): ' + JSON.stringify(this.state.fieldOptions))

                                let result = res.data;
                                let fieldOptions = typeof result['hydra:member']!=='undefined' ? result['hydra:member'] : [];
                                fieldOptionsCount = typeof result['hydra:totalItems']!=='undefined' ? result['hydra:totalItems'] : 0;

                                this.addOrUpdateState(fieldName, fieldOptions);

                                // console.log('Form.js:474 - state.fieldOptions (after): ' + JSON.stringify(this.state.fieldOptions))



                            }
                        })
                    }

            })
        }


        // Show progressbar
        this.setState({ isSubmitted: true });

        // Api Platform - entity - get ID or ID multiples
        let apiFetch = null;
        if (this.state.idMultiple) {
            apiFetch = apiService.findByMultipleIds(this.state.id);

            // Reset validator
            validatorFields = {}


        } else if (this.state.id>0) {
            apiFetch = apiService.findById(this.state.id);
        }


        // if ID or ID multiples
        if (apiFetch !== null) {
            apiFetch.then(res => {
                if (res.status === 200) {

                    // console.log(res.data);

                    let result = res.data;

                    if (!this.state.idMultiple) {
                        this.getSingleResult(result)
                        return;
                    }

                    // Get multiple
                    this.getMultipleResult(result)
                    return;


                }

                this.flashMessage("error", res.data.message)

            }).catch((error) => {
                if (error.response) {
                    // console.log(error.response.data.message);
                    this.flashMessage("error", error.response.data.message)
                } else if (error.request) {
                    console.log(error);
                    console.log(error.request);
                }

            });

            return;
        }

        // Hide progress
        this.setState({ isSubmitted: false });


    }

    flashMessage = (severity, message) => {
        this.setState({message: message});
        this.setState({messageSeverity: severity});
    }

    handleShowAll = () => {
        this.setState({showAll: true});
    }
    handleHideUntil7Items = () => {
        this.setState({showAll: false});
    }

    handleRemoveOneRowMultiple = (id) => {


        // Remove rows
        let rowsUpdated = this.state.rowsMultiple.filter( (value, index, arr) => {
            return id !== value['id'];
        })

        this.setState({rowsMultiple: rowsUpdated})
    }

    handleChangeReactSelect = (e) => {


        if (e.fieldName !== null) {

            // Autocomplete field name
            let fieldName = e.fieldname
            let oldFieldValues = this.state.fieldValues
            oldFieldValues[fieldName] = e;
            this.setState({fieldValues: oldFieldValues})
        }

        return;
    }
    onChange = (e) => {

        let fPatternSearch = ''
        let fPatternReplace = ''
        let valueAddedToOption = ''




        let fieldName = e.target.name;
        let fieldValue = e.target.value;
        let fieldChecked = e.target.checked;
        let fieldType = typeof e.target.type !== 'undefined' ? e.target.type : '';

        // console.log('Form.js:474 - onChange - state.fieldValues (before): ' + JSON.stringify(this.state.fieldValues))

        // if checkbox
        this.props.model.form.fields.map((row, i) => {

            let fType = row.type
            let fName = row.key

            if (fieldName===fName) {
                fPatternSearch = typeof row.submit !== 'undefined' && typeof row.submit.patternSearch !== 'undefined' ? row.submit.patternSearch : ''
                fPatternReplace = typeof row.submit !== 'undefined' && typeof row.submit.patternReplace !== 'undefined' ? row.submit.patternReplace : ''
                valueAddedToOption = typeof row.option !== 'undefined' && typeof row.option.valueAddedTo !== 'undefined' ? row.option.valueAddedTo : ''
            }

        })

        if (fieldType === 'checkbox') {
            valuesInitial[fieldName] = !valuesInitial[fieldName]
            this.setState({fieldValues : valuesInitial})
            return;
        }

        if (fieldType === 'text' && fPatternSearch!=='' && fPatternReplace!=='') {
            // console.log('Form.js:474 - onChange - fieldName (before): ' + JSON.stringify(valuesInitial[fieldName]))
            valuesInitial[fieldName] = fieldValue.replace(fPatternSearch, fPatternReplace);
            // console.log('Form.js:474 - onChange - fieldName (after): ' + JSON.stringify(valuesInitial[fieldName]))
            this.setState({fieldValues : valuesInitial})
            return;
        }


        if (fieldType === 'number' && valueAddedToOption !== '' && parseInt(fieldValue)>=0 ) {
            let oldValue = parseInt(valuesImmutable[valueAddedToOption])>0 ? parseInt(valuesImmutable[valueAddedToOption]) : 0;
            valuesInitial[valueAddedToOption] = parseInt(fieldValue) + oldValue;
        }

            valuesInitial[fieldName] = fieldValue
        this.setState({fieldValues : valuesInitial})

        console.log('Form.js:474 - onChange - state.fieldValues (after): ' + JSON.stringify(valuesInitial))





    }

    handleOpenDialogDate = (fieldName) => {
        this.setState({datefieldName: fieldName})
        this.setState({ openDialogDate: true });
    }
    handleCloseDialogDate = () => {
        this.setState({ openDialogDate: false });
    }

    saveMultiple = (values) => {


        this.setState({valueForm: values})
        this.handleOpenDialog()

    }

    handleSaveDate = (dateSearch) => {
        DatesService.create({date: dateSearch}).then( res => {

            // Http Code 200 or 201
            if ([200, 201].includes(res.status)) {

                let result = res.data;
                let rowsData = result;

                if (rowsData['@id'] !== '') {

                    // Hide progress bar
                    this.setState({isSubmittedSearchDate: false})

                    // Show flashmessage
                    this.flashMessage("success", this.props.t('text.success_date_found'), 'messageDialogSearch')

                    // Close Dialog and update states
                    setTimeout( () => {

                        // Update State date
                        this.setState({dateId: rowsData['@id']})
                        this.setState({date: rowsData['date']});

                        // Hide flash
                        this.flashMessage("", "", 'messageDialogSearch')


                        this.handleCloseDialogDate()
                    },500);

                    return;

                }

            }

        }).catch((error) => {
            if (error.response) {
                console.log(error.response.data.message)
            } else if (error.request) {
                console.log(error.request);
            } else {
                // Something happened in setting up the request and triggered an Error
                console.log(error.message)
            }
        });
    }

    render() {

        const { t, keySuccessSubmitted, keyButtonSave, history, model  } = this.props;

        let pathList = model.list.actions.pathlist
        let apiService = model.service

        for (let obj in this.state.fieldValues) {
                valuesInitial[obj] = this.state.fieldValues[obj]
        }


        // Clone
        valuesInitial['clone'] = 0


        //console.log('Form.js:474 - validatorFields : ' + JSON.stringify(validatorFields))



        return(
            <React.Fragment>

                <Container >

                    <Divider variant="middle" />

                    <Grid container justify="space-around">


                        <Formik
                            enableReinitialize

                            initialValues={
                                valuesInitial
                            }

                            onSubmit={(values, { setSubmitting }) => {
                                setSubmitting(true);

                                values = this.prepareFieldsToSave(values);

                                // alert(JSON.stringify(values))
                                // return;

                                if (values.clone === 1) {
                                    this.saveCloneMultiple(values);
                                    return;
                                }

                                if (this.state.idMultiple) {
                                    this.saveMultiple(values);
                                    return;
                                }

                                this.setState({ isSubmitted: true });

                                let apiPost = null;
                                if (this.state.id>0) {
                                    apiPost = apiService.update(values, this.state.id);
                                } else {
                                    apiPost = apiService.create(values);
                                }

                                if (apiPost !== null) {
                                    apiPost.then(res => {

                                        this.setState({isSubmitted: false});

                                        if ([200, 201].includes(res.status)) {


                                            this.flashMessage("success", t(keySuccessSubmitted))

                                            // hide message after few sec
                                            setTimeout(() => {
                                                this.handleList()
                                            }, 500);

                                        } else {
                                            this.flashMessage("error", res.data.message)
                                        }
                                    }).catch((error) => {
                                        this.setState({isSubmitted: false});
                                        if (error.response) {
                                            this.flashMessage("error", error.response.data.message)
                                        } else if (error.request) {
                                            console.log(error.request);
                                        } else {
                                            // Something happened in setting up the request and triggered an Error
                                            this.flashMessage("error", error.message)
                                        }
                                    });
                                }

                            }}

                            validationSchema={Yup.object().shape(validatorFields)}
                        >
                            {(props) => {
                                const {
                                    touched,
                                    errors,
                                    handleBlur,
                                    handleSubmit,
                                    setFieldValue
                                } = props;
                                return (
                                    <form onSubmit={handleSubmit}>


                                        {
                                            this.state.message !== ''
                                                ?
                                                <Alert severity={this.state.messageSeverity}>{this.state.message}</Alert>
                                                : ''
                                        }

                                        {
                                            this.state.snackmessage !== ''
                                                ?
                                                <Snackbar
                                                    anchorOrigin={{
                                                        vertical: 'bottom',
                                                        horizontal: 'left',
                                                    }}
                                                    open={true}
                                                    autoHideDuration={4000}
                                                    message={this.state.snackmessage}
                                                />
                                                : ''
                                        }

                                        {/* Each fields */}

                                        {
                                            this.props.model.form.fields.map((row, i) => {

                                                let fieldType = row.type
                                                let fieldName = row.key
                                                let fieldLabel = row.label
                                                let fieldOptions = typeof row.option !== 'undefined' ? row.option : []
                                                let fieldOptionsKey = typeof fieldOptions !== 'undefined' ? fieldOptions['key'] : ''
                                                let fieldOptionsValue = typeof fieldOptions !== 'undefined' ? fieldOptions['value'] : ''
                                                let fieldOptionsValueAddedfrom = typeof fieldOptions !== 'undefined' && typeof fieldOptions['valueAddedfrom'] !== 'undefined' ? fieldOptions['valueAddedfrom'] : ''
                                                let fieldAttributes = typeof row.attributes !== 'undefined' ? row.attributes : []
                                                let fieldFilter = typeof row.filter !== 'undefined' ? row.filter : []
                                                let fieldServiceClass = typeof row['serviceClass'] !== 'undefined' ? row['serviceClass'] : null;

                                                let entityList = []


                                                let indexFieldOption = this.findIndexFieldOption(fieldName)


                                                let valueFieldState = (typeof this.state.fieldOptions[indexFieldOption] !== 'undefined' ? this.state.fieldOptions[indexFieldOption][fieldName] : '');


                                                if (typeof valueFieldState !== 'undefined') {
                                                    entityList = valueFieldState ;
                                                }

                                                // console.log('Form.js:474 - fieldName : ' + JSON.stringify(fieldName) + ' - entityList : ' + JSON.stringify(entityList))
                                                // console.log('Form.js:474 - fieldName : ' + JSON.stringify(fieldName) + ' - valuesInitial : ' + JSON.stringify(valuesInitial[fieldName]))

                                                switch (fieldType) {

                                                    case 'autocomplete':
                                                        return (
                                                            <React.Fragment>
                                                                <AutocompleteField
                                                                    placeholder={t(fieldLabel)}
                                                                    name={t(fieldName)}
                                                                    value={valuesInitial[fieldName]}
                                                                    options={[valuesInitial[fieldName]]}
                                                                    service={fieldServiceClass}
                                                                    onChange={this.handleChangeReactSelect}
                                                                />
                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>
                                                            </React.Fragment>
                                                        )
                                                        break;

                                                    case 'switch':
                                                        return (
                                                            <React.Fragment>
                                                                <Field name={`${fieldName}`} >
                                                                    {({ field, form, meta }) => (
                                                                        <FormControlLabel
                                                                            control={<Switch
                                                                                {...field}
                                                                                checked={valuesInitial[fieldName]}
                                                                                onChange={this.onChange}
                                                                                />}
                                                                            label={t(fieldLabel)}
                                                                        />)}
                                                                </Field>
                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>
                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    case 'nativeEntity':
                                                        return (
                                                            <React.Fragment>
                                                                <FormControl margin="normal" fullWidth variant="outlined" >
                                                                    <InputLabel >{t(fieldLabel)}</InputLabel>
                                                                    <Field name={`${fieldName}`}>
                                                                        {({ field, form, meta }) => (
                                                                            <Select
                                                                                {...field}
                                                                            >
                                                                            {
                                                                                entityList.map( (row) => {
                                                                                    return (
                                                                                        <MenuItem value={row[fieldOptionsKey]}>{row[fieldOptionsValue]}</MenuItem>
                                                                                    )
                                                                                })

                                                                            }
                                                                            </Select>
                                                                        )}
                                                                    </Field>
                                                                    <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                        {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                    </p>
                                                                </FormControl>
                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    case 'relation':

                                                        return (
                                                            <React.Fragment>
                                                                <FormControl margin="normal" fullWidth variant="outlined" >
                                                                    <InputLabel >{t(fieldLabel)}</InputLabel>
                                                                    <Field name={`${fieldName}`}>
                                                                    {({ field, form, meta }) => (
                                                                            <Select
                                                                                {...field}
                                                                            >
                                                                                {
                                                                                    this.state.fieldOptions[indexFieldOption][fieldName].map( (row) => {
                                                                                        return (
                                                                                            <MenuItem value={row}>{row}</MenuItem>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </Select>
                                                                    )}
                                                                </Field>
                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>
                                                                </FormControl>
                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    case 'translation':

                                                        return (
                                                            <React.Fragment>

                                                                <Field name={`${fieldName}`}>
                                                                    {({ field, form, meta }) => (

                                                                        <FormControl
                                                                            margin="normal"
                                                                            fullWidth
                                                                            variant="outlined"
                                                                        >
                                                                            <InputLabel >{t(fieldLabel)}</InputLabel>
                                                                            <Select
                                                                                {...field}
                                                                                margin="normal"
                                                                            >
                                                                                <MenuItem value="">{t('field.select_none')}</MenuItem>
                                                                                {
                                                                                    entityList.map( (row) => {
                                                                                        return (
                                                                                            <MenuItem value={row[fieldOptionsKey]}>{ Formatter.format(row, {options: fieldFilter['options'] }) }</MenuItem>
                                                                                        )
                                                                                    })

                                                                                }
                                                                            </Select>
                                                                        </FormControl>
                                                                    )}
                                                                </Field>
                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>

                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    case 'datecreator':
                                                        return (
                                                            <React.Fragment>
                                                                <Button
                                                                    fullWidth
                                                                    onClick={() => this.handleOpenDialogDate(fieldName)}
                                                                >
                                                                    {
                                                                         valuesInitial[fieldName][fieldOptionsKey] !== '' ?

                                                                           (
                                                                                <React.Fragment>
                                                                                {t("entity.data_date_type.header.field_date")} :
                                                                                    <Moment format={fieldOptions['format']}>{ valuesInitial[fieldName][fieldOptionsValue] }</Moment>
                                                                                    ({ valuesInitial[fieldName][fieldOptionsKey] }) - {t("field.button_choose_other_date")}
                                                                                </React.Fragment>
                                                                           )
                                                                            :
                                                                           (
                                                                                <React.Fragment>{t("field.button_choose_date")}</React.Fragment>
                                                                           )
                                                                    }
                                                                </Button>
                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>
                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    case 'date':
                                                        return (
                                                            <React.Fragment>
                                                                {
                                                                    fieldAttributes.includes('readonly') ?
                                                                        // Readonly
                                                                        (
                                                                            <React.Fragment>
                                                                                <Typography paragraph >
                                                                                    {t(fieldLabel)} :
                                                                                    <strong>
                                                                                        <Moment format={fieldOptions['format']}>
                                                                                            {valuesInitial[fieldName]}
                                                                                        </Moment>
                                                                                    </strong>
                                                                                </Typography>
                                                                            </React.Fragment>
                                                                        )
                                                                        :
                                                                        (
                                                                            <React.Fragment>
                                                                                <Field name={`${fieldName}`}>
                                                                                    {({ field, form, meta }) => (
                                                                                        <DateField
                                                                                            labelkeytranslate={t(fieldLabel)}
                                                                                            disabled={this.state.isSubmitted}
                                                                                            {...field}
                                                                                            format={fieldOptions['format']}
                                                                                            variant="inline"
                                                                                            autoOk={fieldOptions['autoOk']}
                                                                                            fullWidth={fieldOptions['fullWidth']}
                                                                                        />
                                                                                    )}
                                                                                </Field>
                                                                            </React.Fragment>
                                                                        )
                                                                }
                                                            </React.Fragment>
                                                        )
                                                        break;
                                                    default:
                                                    case 'text':
                                                        return (
                                                            <React.Fragment>

                                                                <FormControl margin="normal" fullWidth variant="outlined" >
                                                                    <InputLabel >{t(fieldLabel)}</InputLabel>

                                                                        <Field name={`${fieldName}`}>
                                                                            {({ field, form, meta }) => (
                                                                                <React.Fragment>
                                                                                <TextField
                                                                                    autoFocus={true}
                                                                                    variant="outlined"
                                                                                    type={fieldAttributes.includes('number') ? 'number' : 'text'}
                                                                                    margin="normal"
                                                                                    {...field}
                                                                                    disabled={this.state.isSubmitted}
                                                                                    onChange={this.onChange}
                                                                                    fullWidth
                                                                                />
                                                                                    { fieldOptionsValueAddedfrom !== '' && typeof valuesImmutable[fieldName] !== 'undefined' && valuesImmutable[fieldName] !== valuesInitial[fieldName] ?
                                                                                        (<span>{ t('tooltip.value_from_db') + ' : ' + valuesImmutable[fieldName]}</span>) : '' }
                                                                                </React.Fragment>)}
                                                                        </Field>
                                                                </FormControl>

                                                                <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                                                    {errors[fieldName] && touched[fieldName] && errors[fieldName]}
                                                                </p>

                                                                <Divider variant="middle" />

                                                            </React.Fragment>
                                                        )
                                                        break;

                                                }

                                            })
                                        }




                                        {
                                            this.state.isSubmitted
                                                ?
                                                <LinearProgress margin="normal"/> : ''
                                        }

                                        <br />

                                        <Button
                                            fullWidth
                                            disabled={this.state.isSubmitted}
                                            type="submit"
                                            variant="contained"
                                            color="secondary" >
                                            {
                                                this.state.isSubmitted
                                                    ?
                                                    t('text.loading')
                                                    :
                                                    t(keyButtonSave)
                                            }
                                        </Button>

                                        {
                                            this.state.rowsMultiple.length > 0 ?
                                                (
                                                    <React.Fragment>
                                                        <br /><br />
                                                        <Button
                                                            margin="normal"
                                                            fullWidth
                                                            disabled={this.state.isSubmitted}
                                                            type="button"
                                                            onClick={() => {
                                                                setFieldValue('clone', 1, false)
                                                                handleSubmit()
                                                            }}
                                                            variant="contained"
                                                            color="default" >
                                                            {
                                                                t('field.button_createnew_all')
                                                            }
                                                        </Button>
                                                    </React.Fragment>
                                                )
                                                :
                                                ''
                                        }

                                        <br />
                                        <Tooltip title={t('tooltip.back')}>
                                            <IconButton aria-label="delete" onClick={this.handleList}>
                                                <ArrowBackIcon />
                                            </IconButton>
                                        </Tooltip>

                                    {/* End of fields */}

                                    </form>
                                );
                            }}
                        </Formik>
                        {
                            this.state.rowsMultiple ?
                                (
                                    <React.Fragment>
                                        <List >
                                            {
                                                this.state.rowsMultiple.map( (row, index) => {

                                                    let fieldPrimaryContent = this.findContentField(fieldInsidePrimary, row)

                                                    let fieldSecondaryContent = this.findContentField(fieldInsideSecondary, row)

                                                    // fieldPrimaryContent = 'fieldPrimaryContent '
                                                    // fieldSecondaryContent = 'fieldSecondaryContent '

                                                    if (index < 7) {
                                                        return (
                                                            <React.Fragment>
                                                                <ListItem>
                                                                    <Tooltip title={fieldPrimaryContent}>
                                                                        <ListItemText primary={_.truncate( fieldPrimaryContent, {'length': 45, 'separator': ' '})}
                                                                                      secondary={<span>{ fieldSecondaryContent }</span>}/>
                                                                    </Tooltip>

                                                                    <Tooltip title={t('tooltip.remove_from_list')}>
                                                                        <ListItemSecondaryAction>
                                                                            <IconButton onClick={() => this.handleRemoveOneRowMultiple(row.id)} edge="end" aria-label="delete">
                                                                                <DeleteIcon />
                                                                            </IconButton>
                                                                        </ListItemSecondaryAction>
                                                                    </Tooltip>
                                                                </ListItem>
                                                                <Divider variant="middle"/>
                                                            </React.Fragment>
                                                        )
                                                    }

                                                    if (!this.state.showAll && index === 7 && this.state.rowsMultiple.length > 7) {
                                                        return (
                                                            <React.Fragment>
                                                                <div  align="center"><br />
                                                                    <Button
                                                                        onClick={this.handleShowAll}
                                                                        variant="contained"
                                                                        color="primary"
                                                                        size="large"
                                                                        endIcon={<ExpandMoreIcon />}
                                                                    >
                                                                        { t('field.button_showmore', {count: this.state.rowsMultiple.length} ) }
                                                                    </Button>
                                                                </div>
                                                            </React.Fragment>
                                                        )
                                                    }

                                                    if (index > 7 && this.state.showAll && index < this.state.rowsMultiple.length-1) {
                                                        return (
                                                            <React.Fragment>
                                                                <ListItem>
                                                                    <Tooltip title={fieldPrimaryContent}>
                                                                        <ListItemText primary={<span >{_.truncate(fieldPrimaryContent, {'length': 50, 'separator': ' '})}</span>}
                                                                                      secondary={<span>{fieldSecondaryContent}</span>}/>
                                                                    </Tooltip>

                                                                    <Tooltip title={t('tooltip.remove_from_list')}>
                                                                        <ListItemSecondaryAction>
                                                                            <IconButton onClick={() => this.handleRemoveOneRowMultiple(row.id)} edge="end" aria-label="delete">
                                                                                <DeleteIcon />
                                                                            </IconButton>
                                                                        </ListItemSecondaryAction>
                                                                    </Tooltip>

                                                                </ListItem>
                                                                <Divider variant="middle"/>
                                                            </React.Fragment>
                                                        )
                                                    }

                                                    if (index > 7 && this.state.showAll && index === this.state.rowsMultiple.length-1) {
                                                        return (
                                                            <React.Fragment>
                                                                <div  align="center"><br />
                                                                    <Button
                                                                        onClick={this.handleHideUntil7Items}
                                                                        variant="contained"
                                                                        color="primary"
                                                                        size="large"
                                                                        endIcon={<ExpandLessIcon />}
                                                                    >
                                                                        { t('field.button_showless'  ) }
                                                                    </Button>
                                                                </div>
                                                            </React.Fragment>
                                                        )
                                                    }


                                                })

                                            }
                                        </List>
                                    </React.Fragment>
                                )  : (
                                    <React.Fragment>No rows</React.Fragment>
                                )
                        }
                    </Grid>
                </Container>

                {/* Dialog confirm multiple save */}
                <Dialog
                    open={this.state.openDialog}
                    onClose={this.handleCloseDialog}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{ t('text.dialog_confirm_title') }</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            { t('text.dialog_confirm_savemultiple') }
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleOkDialog} color="primary">
                            { t('field.button_yes') }
                        </Button>
                        <Button onClick={this.handleCloseDialog} color="primary" autoFocus>
                            { t('field.button_no') }
                        </Button>
                    </DialogActions>
                </Dialog>

                {/* Dialog search and add date */}
                <Dialog
                    open={this.state.openDialogDate}
                    onClose={this.handleCloseDialogDate}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{ t('text.dialog_add_date') }</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">

                            <Formik
                                enableReinitialize

                                initialValues={
                                    {
                                        'dateSearch': '',
                                    }
                                }

                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(true);

                                    // alert(this.state.datefieldName);return; // TODO Remove


                                    if (values.dateSearch !== '') {

                                        let dateConverted = moment(values.dateSearch).format('YYYY-MM-DD');

                                        // Show progress bar
                                        this.setState({isSubmittedSearchDate: true})

                                        DatesService.filterBy(dateConverted,'date').then(res => {

                                            // Http Code 200 or 201
                                            if ([200, 201].includes(res.status)) {

                                                let result = res.data;
                                                let rowsData = result['hydra:member'];
                                                let totalItems = result['hydra:totalItems'];

                                                if (totalItems>0 && rowsData[0]['@id'] !== '') {

                                                    // Hide progress bar
                                                    this.setState({isSubmittedSearchDate: false})

                                                    // Show flashmessage
                                                    this.flashMessage("success", t('text.success_date_found'), 'messageDialogSearch')

                                                    // Close Dialog and update states
                                                    setTimeout( () => {

                                                        let optionField = this.findOptionField(this.state.datefieldName)


                                                        // if create > convert to object
                                                        if (typeof valuesInitial[this.state.datefieldName] === 'string') {
                                                            valuesInitial[this.state.datefieldName] = {}
                                                        }

                                                        valuesInitial[this.state.datefieldName][optionField['key']] = rowsData[0][optionField['key']]
                                                        valuesInitial[this.state.datefieldName][optionField['value']] = rowsData[0][optionField['value']]

                                                        this.setState({ fieldValues: valuesInitial})

                                                        // Hide flash
                                                        this.flashMessage("", "", 'messageDialogSearch')


                                                        this.handleCloseDialogDate()
                                                    },200);

                                                    return;

                                                }

                                                // Save date in Database
                                                this.handleSaveDate(values.dateSearch);

                                            }

                                        }).catch((error) => {
                                            if (error.response) {
                                                console.log(error.response.data.message)
                                            } else if (error.request) {
                                                console.log(error.request);
                                            } else {
                                                // Something happened in setting up the request and triggered an Error
                                                console.log(error.message)
                                            }
                                        });
                                    }

                                    return;
                                }}

                                validationSchema={Yup.object().shape({})}
                            >

                                {(props) => {
                                    const {
                                        handleSubmit
                                    } = props;
                                    return (
                                        <form onSubmit={handleSubmit}>

                                            {
                                                this.state.messageDialogSearch !== ''
                                                    ?
                                                    <Alert severity={this.state.messageDialogSearchSeverity}>{this.state.messageDialogSearch}</Alert>
                                                    : ''
                                            }

                                            <DateField
                                                labelkeytranslate={t('field.button_choose_date')}
                                                name="dateSearch"
                                                format="dd-MM-yyyy"
                                                variant="inline"
                                                autoOk={true}
                                                fullWidth={true}
                                            />

                                            {
                                                this.state.isSubmittedSearchDate
                                                    ?
                                                    <LinearProgress margin="normal"/> : ''
                                            }

                                            <DialogActions>
                                                <Button type="submit" color="primary">
                                                    { t('field.button_search_date') }
                                                </Button>
                                                <Button color="primary" autoFocus onClick={this.handleCloseDialogDate}>
                                                    { t('field.button_cancel') }
                                                </Button>
                                            </DialogActions>

                                        </form>
                                    );
                                }}
                            </Formik>


                        </DialogContentText>
                    </DialogContent>

                </Dialog>

            </React.Fragment>
        )
    }



}

export default withTranslation()(Form);
