import React from 'react';
import AsyncSelect from 'react-select/async';



class AutocompleteField extends React.Component {

    constructor(props) {
        super(props);
    }

    filterResults(inputValue)  {

        if (!inputValue) {
            return Promise.resolve({ options: [] });
        }
        return this.props.service.filterBy(inputValue, 'name').then(res => {

            if ([200, 201].includes(res.status)) {

                // build
                let optionsResult = [];

                res.data['hydra:member'].map(row => {
                    optionsResult.push({value: row.id, label: row.name, fieldname: this.props.name});
                    return true;
                });

                return optionsResult;

            }
        }).catch((error) => {

        });
    }

    getAsyncOptions(inputValue) {
        return new Promise((resolve, reject) => {
            resolve(this.filterResults(inputValue));
        });
    }


    render() {
        return (
            <React.Fragment>
                <AsyncSelect
                    placeholder={this.props.placeholder}
                    name={this.props.name}
                    value={this.props.value}
                    options={this.props.options}
                    onChange={this.props.onChange}
                    isClearable
                    loadOptions={inputValue => this.getAsyncOptions(inputValue)}
                />
            </React.Fragment>
        )
    }

}



export default AutocompleteField;
