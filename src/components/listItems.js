import React from 'react';

import DashboardIcon from '@material-ui/icons/Dashboard';
import StorageIcon from '@material-ui/icons/Storage';
import PlaceIcon from '@material-ui/icons/Place';
import SettingsIcon from '@material-ui/icons/Settings';
import TranslateIcon from '@material-ui/icons/Translate';
import PersonIcon from '@material-ui/icons/Person';

export const SidebarMenu = [
    {
        "id": "i_0",
        "key": "home",
        "label": "",
        "items": [
            {
                "id": "i_01",
                "label": "dashboard",
                "icon": <DashboardIcon />,
                "to": "/home"
            }
        ]
    },
    {
        "id": "i_1",
        "key": "data",
        "label": "data_types",
        "items": [
            {
                "id": "i_12",
                "label": "data_types",
                "icon": <StorageIcon />,
                "to": "/data_types",
                "items": [
                    {
                        "id": "i_121",
                        "label": "data_provinces",
                        "icon": <PlaceIcon />,
                        "to": "/data_provinces"
                    },
                    {
                        "id": "i_122",
                        "label": "data_regions",
                        "icon": <PlaceIcon />,
                        "to": "/data_regions"
                    },
                    {
                        "id": "i_123",
                        "label": "data_districts",
                        "icon": <PlaceIcon />,
                        "to": "/data_districts"
                    },
                    {
                        "id": "i_124",
                        "label": "data_sexes",
                        "icon": <PersonIcon />,
                        "to": "/data_sexes"
                    },
                ]
            },
            {
                "id": "i_13",
                "label": "data_date_types",
                "icon": <StorageIcon />,
                "to": "/data_date_types",
                "items": [
                    {
                        "id": "i_121",
                        "label": "data_date_provinces",
                        "icon": <PlaceIcon />,
                        "to": "/data_date_provinces"
                    },
                    {
                        "id": "i_122",
                        "label": "data_date_regions",
                        "icon": <PlaceIcon />,
                        "to": "/data_date_regions"
                    },
                    {
                        "id": "i_123",
                        "label": "data_date_districts",
                        "icon": <PlaceIcon />,
                        "to": "/data_date_districts"
                    }
                ]
            },
        ]
    },
    {
        "id": "i_2",
        "key": "place",
        "label": "Places",
        "items": [
            {
                "id": "i_21",
                "label": "provinces",
                "icon": <PlaceIcon />,
                "to": "/provinces"
            },
            {
                "id": "i_22",
                "label": "regions",
                "icon": <PlaceIcon />,
                "to": "/regions"
            },
            {
                "id": "i_23",
                "label": "districts",
                "icon": <PlaceIcon />,
                "to": "/districts"
            },
            {
                "id": "i_24",
                "label": "communes",
                "icon": <PlaceIcon />,
                "to": "/communes"
            },
            {
                "id": "i_25",
                "label": "quartiers",
                "icon": <PlaceIcon />,
                "to": "/quartiers"
            },
        ]
    },
    {
        "id": "i_3",
        "key": "extra",
        "label": "Extra",
        "items": [
            {
                "id": "i_31",
                "label": "translations",
                "icon": <TranslateIcon />,
                "to": "/translations"
            },
            {
                "id": "i_32",
                "label": "settings",
                "icon": <SettingsIcon />,
                "to": "/settings"
            },
        ]
    }
];
