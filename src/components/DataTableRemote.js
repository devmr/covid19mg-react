import React, {useEffect, useRef, useState} from 'react';
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Checkbox from "@material-ui/core/Checkbox";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "./TablePaginationActions";
import {Config} from "../utils/config";
import { Formatter } from "../utils/formatter";
import TableBody from "@material-ui/core/TableBody";
import CircularProgress from "@material-ui/core/CircularProgress";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import AuthService from "../service/AuthService";
import SessionService from "../service/SessionService";
import axios from 'axios';
import Alert from "@material-ui/lab/Alert/Alert";
import Link from "@material-ui/core/Link";
import {useTranslation} from "react-i18next";
import Snackbar from "@material-ui/core/Snackbar";
import { useHistory } from "react-router-dom";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import SearchIcon from '@material-ui/icons/Search';
import { Formik} from "formik";
import * as Yup from "yup";
import Filterfield from "./FilterField";
import strings from "locutus/php/strings"
import moment from "moment";
import { useLocation } from 'react-router-dom';
import paramQueryString from 'query-string'
import AutocompleteField from "./AutocompleteField";
const apiUrl = Config.host();


const DataTableRemote = (props) => {

    const location = useLocation();

    //
    const [rows, setRows] = useState([]);
    const [selected, setSelected] = useState([]);
    const [idChosen, setIdChosen] = useState(null);
    const [rowsPerPage, setRowsPerPage] = useState(30);
    const [page, setPage] = useState(0);
    const [isLoading, setisLoading] = useState(true);

    // Pagination
    const [totalItems, settotalItems] = useState(0);
    const [firstPageUrl, setFirstPageUrl] = useState('');
    const [previousPageUrl, setpreviousPageUrl] = useState('');
    const [nextPageUrl, setnextPageUrl] = useState('');
    const [lastPageUrl, setlastPageUrl] = useState('');

    const [openDialog, setOpenDialog] =  useState(false);
    const [openDialogFilter, setOpenDialogFilter] =  useState(false);

    const [error_message, setErrorMessage] =  useState('');
    const [error_message_severity, setErrorSeverity] =  useState('');
    const [openSnackbar, setOpenSnackbar] =  useState(true);

    // Default sorting
    const [sort, setSort] = useState('asc');
    const [fieldSorted, setFieldSorted] = useState('');

    // Filter
    const [filter, setFilter] = useState([]);
    const [filterSearch, setFilterSearch] = useState([]);

    // QueryString
    const [queryString, setQueryString] = useState([]);

    // Autocomplete
    const [filterAutocompleteValue, setFilterAutocompleteValue] = useState([]);

    const { t } = useTranslation();

    const history = useHistory();


    const { headerCell, service, actions } = props;

    const pathApiUrl = typeof actions !== 'undefined' && actions.api !== null ? actions.api : '';
    const pathEdit = typeof actions !== 'undefined' && actions.edit !== null ? actions.edit : '';
    const pathEditMultiple = typeof actions !== 'undefined' && actions.editmultiple !== null ? actions.editmultiple : '';
    const duplicateEnable = typeof actions !== 'undefined' && actions.duplicate  === true;
    const deleteEnable = typeof actions !== 'undefined' && actions.delete === true ;
    const pathCreate = typeof actions !== 'undefined' && actions.create !== null ? actions.create : '';
    const actionsEnable = pathEditMultiple !== '' || deleteEnable || duplicateEnable ;

    let filterEnabled = false;
    let keyList = {};

    const findDefaultValue = (filterSearch, value) => {
        let found = '';
        filterSearch.map( (row) => {
            for(let obj in row) {
                if (obj == value) {
                    found = row[obj];
                }
            }
        });

        return found;
    }

    headerCell.map((row, index) => {

        if (typeof row.filter !== 'undefined' && row.filter) {
            let fieldType = row.filter.type;
            let fieldKey = row.key;
            keyList[`${fieldType}_${fieldKey}`] = findDefaultValue(filterSearch, `${fieldType}_${fieldKey}`);
            keyList[`option${fieldType}_${fieldKey}`] = findDefaultValue(filterSearch, `option${fieldType}_${fieldKey}`);;
            filterEnabled = true;
        }



    });

    // Check all rows
    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = rows.map((n) => n.id);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };


    // Change page - paginate
    const handleChangePage = (event, newPage) => {
        let url = '';
        let paramQs = '';

        switch (true) {

            default:
            case (newPage-page) === 1 :
                url = apiUrl + nextPageUrl;
                paramQs = nextPageUrl.substr(nextPageUrl.indexOf('?')+1);
                break;
            case (newPage-page) === -1 :
                url = apiUrl + previousPageUrl;
                paramQs = previousPageUrl.substr(previousPageUrl.indexOf('?')+1);
                break;
            case (newPage-page) > 1 :
                url = apiUrl + lastPageUrl;
                paramQs = lastPageUrl.substr(lastPageUrl.indexOf('?')+1);
                break;
            case (newPage-page) < -1 :
                url = apiUrl + firstPageUrl;
                paramQs = firstPageUrl.substr(firstPageUrl.indexOf('?')+1);
                break;
        }

        // Change state of current page
        setPage(newPage);

        // alert(location.pathname +  '?' + paramQs)


        // Go to url
        fetchUrl(url)

        // Change url
        history.push(location.pathname +  '?' + paramQs)

        // Keep url in localStorage
        SessionService.store("referrer", location.pathname +  '?' + paramQs)
        SessionService.store("api_url", url)


    }

    const handleChangeRowsPerPage = (event) => {
        let pageChoosen = parseInt(event.target.value, 10);
        setRowsPerPage(pageChoosen);
        setPage(0);

        let url = apiUrl + pathApiUrl + '?itemsPerPage=' + pageChoosen;

        fetchUrl(url)

        let paramQs = 'itemsPerPage='  + pageChoosen;

        // Change url
        history.push(location.pathname +  '?' + paramQs)

        // Keep url in localStorage
        SessionService.store("referrer", location.pathname +  '?' + paramQs)
        SessionService.store("api_url", url)
    };

    const flashMessage = (severity, message) => {
        setOpenSnackbar(true);
        setErrorMessage(message);
        setErrorSeverity(severity);

        setTimeout( () => {
            setOpenSnackbar(false);
        },4000);
    }

    const fetchEntityFilter = () => {
        headerCell.map( (row) => {
            if (typeof row.filter !== 'undefined' && ['relation', 'translation', 'nativeEntity'].includes(row.filter.type)) {

                let service = row.filter.service;

                if (service === null) {
                    return;
                }


                service.findAll().then(res => {

                    if(res.status === 200){



                        let result = res.data;
                        let rowsData = result['hydra:member'];
                        let totalItems = result['hydra:totalItems'];

                        let filterAdded = filter;
                        filterAdded.push({ key: row.key, rows: rowsData, totalItems:  totalItems})
                        setFilter(filterAdded);

                    }
                }).catch((error) => {
                    if (error.response) {
                        console.log(error.response.data.message);
                    } else if (error.request) {
                        console.log(error);
                        console.log(error.request);
                    }
                });

            }
        });
    }

    const prepareFieldsToSave = (values) => {

        // Mapping Field autocomplete and API URL
        const mappingApiAutocompletefield = Config.getMappingApiAutocompletefield();

        if (filterAutocompleteValue.length) {
            filterAutocompleteValue.map( row => {
                let currentFieldName = row['fieldname']
                let currentFieldValue = row['value']
                values[`${currentFieldName}`] = strings.sprintf(mappingApiAutocompletefield[`${currentFieldName}`], currentFieldValue);
            })
        }

        // console.log('fieldSubmit: (filterAutocompleteValue): ' + JSON.stringify(filterAutocompleteValue));
        // console.log('fieldSubmit: (values): ' + JSON.stringify(values));

        return values;

    }

    const refreshToken = () => {
        let info = JSON.parse(SessionService.get("userInfo"));

        if (info==='') {
            return;
        }

        let data = { refresh_token: info.refresh_token};
        AuthService.refreshToken(data).then(res => {
            if(res.status === 200){
                // keep remember be
                if (AuthService.getRememberMe()) {
                    res.data['rememberme'] = true;
                }

                SessionService.store("userInfo", JSON.stringify(res.data));
            }
            return;
        });

    }

    // Fetch remote url
    const fetchUrl = (url) => {

        // Show progress bar
        setisLoading(true);

        url = typeof url !== 'undefined' ? url : '';

        let urlService = service.findAll();
        if (url !== '') {
            urlService = axios.get(url, SessionService.getAuthHeader());
        }

        urlService.then(res => {
            if(res.status === 200){

                let result = res.data;
                let rows = result['hydra:member'];
                let totalItems = result['hydra:totalItems'];
                let firstPageUrl = typeof result['hydra:view'] !== 'undefined' ? isset(result['hydra:view']['hydra:first']) : '';
                let previousPageUrl = typeof result['hydra:view'] !== 'undefined' ? isset(result['hydra:view']['hydra:previous']) : '';
                let nextPageUrl = typeof result['hydra:view'] !== 'undefined' ? isset(result['hydra:view']['hydra:next']) : '';
                let lastPageUrl = typeof result['hydra:view'] !== 'undefined' ? isset(result['hydra:view']['hydra:last']) : '';

                setRows(rows);
                settotalItems(totalItems);
                setFirstPageUrl(firstPageUrl);
                setpreviousPageUrl(previousPageUrl)
                setnextPageUrl(nextPageUrl)
                setlastPageUrl(lastPageUrl)
                setisLoading(false)

            } else {
                flashMessage("error", res.data.message)
            }
        }).catch((error) => {
            if (typeof error.response  !== 'undefined' && typeof error.response.data !== 'undefined' && [401].includes(error.response.data.code)) {
                setisLoading(false)

                AuthService.logOut()

                // history.push('/#')
                window.location.href = '/'; // TODO replace by push


            } else if (error.request) {
                console.log(error);
                console.log(error.request);
            } else {
                setisLoading(false)
            }
        });


    }

    const isset = (s) => {
        return typeof s !== 'undefined' ? s : '';
    }

    const isItemSelected = (name) => {
        return selected.indexOf(name) !== -1;
    }

    // Reset search
    const resetFilterAndStorage = () => {

        // reset Autocomplete
        setFilterAutocompleteValue([])
        
        // Unset state search
        setFilterSearch([])

        // Remove Storage
        SessionService.removeAll(['referrer', 'api_url', 'filter_search', 'query_string']);

        // Reset url
        history.push(location.pathname)

    }

    // On Load
    const init = () => {

        // if remember me -> keep logged in
        if (SessionService.getRememberMe()) {
            refreshToken()
        }

        // Query String
        const values = paramQueryString.parse(location.search)

        let i =0;
        for (let obj in values) {
            i++;
        }

        // keep in Local Storage if query string is not null
        if (i>0 && SessionService.get('api_url') !== '') {

            fetchUrl(SessionService.get('api_url'))
            fetchEntityFilter()

            // Fill field in dialog filter
            if (SessionService.get('filter_search')!=='') {
                setFilterSearch(JSON.parse(SessionService.get('filter_search')))
            }

            // Set page if query string page
            if (values['page']>0) {
                setPage(values['page'])
            }

            // if show per page
            if (values['itemsPerPage']>0) {
                setPage(0)
                setRowsPerPage(values['itemsPerPage'])
            }


            return;
        }



        // Reset filter - remove keeped storage
        resetFilterAndStorage()

        // Refresh page
        fetchUrl()

        fetchEntityFilter()
    }

    // 2nd param [], run once
    useEffect(() => {
        init()
    }, [pathApiUrl]);

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }


        setSelected(newSelected);
    };

    const handleOpenDialog = () => {
        setOpenDialog(true);
    };

    const handleDeleteSelected = () => {
        handleOpenDialog()
    }

    const handleDeleteOne = (id) => {
        handleOpenDialog()
        setIdChosen(id);
    }

    // edit action
    const  doEdit = (id) => {
        let idList = [];

        // What button is set prior
        if (id !== null) {
            // add idChosen to list
            idList.push(id);
        }

        if (id === null && selected !== null) {
            idList = selected;
        }

        if (idList.length<=0) {
            idList.push('*');
        }

        // if one row
        history.push((idList.length > 1 || idList.includes('*') ? pathEditMultiple : pathEdit) + idList.join(','));
    }

    // Opne dialog filter
    const doSearchAndFilter = () => {
        // console.log(JSON.stringify(filterSearch))
        setOpenDialogFilter(true)
    }

    // Copy action
    const doCopy = (id) => {
        let idList = [];

        // What button is set prior
        if (id !== null) {
            // add idChosen to list
            idList.push(id);
        }

        if (id === null) {
            idList = selected;
        }

        flashMessage("info",t('text.loading_task'))

        // API
        idList.map( (id) => {
            // console.log('copy 2: ' + id)
            service.duplicate(id).then(res => {
                if(res.status === 200){

                    let result = res.data;

                    rows.push(result)

                    setSelected([])

                    flashMessage("success",t('text.success_duplicated', {count: idList.length}))
                    setRows(rows);
                    return;

                }

                let errorMsg = 'Unknown error';

                if ([500,503].includes(res.status) && typeof res.data['hydra:description'] !== 'undefined') {
                    errorMsg = res.data['hydra:description'];
                } else {
                    errorMsg = res.data.message;
                }

                if (errorMsg !== ''){
                    flashMessage("error", errorMsg)
                }

            }).catch((error) => {
                if (error.response) {
                    // console.log(error.response.data.message);
                    flashMessage("error", error.response.data.message)
                } else if (error.request) {

                } else {
                }
            });

            return id;


        })


    }

    // Close Confirm Dialog
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    // Close Dialog Filter
    const handleCloseDialogFilter = () => {
        setOpenDialogFilter(false);
    };

    // Reset filter search
    const handleResetFilter = () => {

        // Reset filter - remove keeped storage
        resetFilterAndStorage()

        // Refresh page
        fetchUrl()

        // Close dialog
        handleCloseDialogFilter()
    };

    // Click OK on dialog confirm
    const handleOkDialog = () => {

        let idList = [];

        // Close this dialog
        handleCloseDialog()

        // if not null --> edit single mode
        if (idChosen !== null) {
            // add idChosen to list
            idList.push(idChosen);
        }

        //  if null --> edit multiple mode
        if (idChosen === null) {
            idList = selected;
        }

        // API
        idList.map( (id) => {
            service.remove(id);
            return id;
        })

        // Remove rows
        let rowsUpdated = removeRows(idList);

        // Update State
        setRows(rowsUpdated);

        let numRemoved = idList.length

        // reset after
        setIdChosen(null);

        // Remove selected
        setSelected([])

        // Flash Message
        flashMessage("success",t('text.success_removed', {count: numRemoved}))
    }

    // Remove rows from state
    const removeRows = (idList) => {

        // Remove rows
        let rowsUpdated = rows.filter( (value, index, arr) => {
            return !idList.includes(value['id']);
        })

        return rowsUpdated;
    }

    // Sort column
    const doSortBy = (field, sort) => {

        setFieldSorted(field);
        setSort(sort === 'asc' ? 'desc' : 'asc');

        let url = apiUrl + pathApiUrl + '?order[' + field + ']=' + sort;

        // If queryString
        if (queryString.length > 0) {
            url += '&' + queryString.join('&');
        }

        // Go to url
        fetchUrl(url)
    }

    const handleChangeReactSelect = (e) => {


        if (e.fieldName !== null) {

            // Autocomplete field name
            let fieldName = e.fieldname

            // Autocomplete field state array
            let dataValue = filterAutocompleteValue;

            // Add to array if true
            let added = filterAutocompleteValue.length === 0;

            // Keep list field name
            let listFieldname = [];
            if (dataValue.length > 0) {
                dataValue.map((row, i) => {
                    listFieldname.push(row.fieldname)
                })
            }

            // Change or add state
            if (dataValue.length > 0) {
                dataValue.map((row, i) => {
                    let currentFieldName = dataValue[i]['fieldname'];
                    let currentField = dataValue[i];
                    if (listFieldname.includes(fieldName) && fieldName == currentFieldName) {
                        dataValue[i] = e;
                    }

                    if (!listFieldname.includes(fieldName)) {
                        added = true;
                    }
                })
            }

            if (added) {
                dataValue.push(e)
            }

            console.log('fieldSubmit: (dataValue): ' + JSON.stringify(dataValue));

            // Update state
            setFilterAutocompleteValue(dataValue);
        }

        return;
    };

    const numSelected = selected.length;

    return (
        <React.Fragment>
            {

                <div style={{ width: '100%' }}>



                    <Box display="flex" p={1} bgcolor="background.paper">
                        <Box  style={{ padding: '4px' }}  p={1} flexGrow={1} >
                            {
                                numSelected > 0 ?
                                 t('text.entityselected', {count: numSelected}) : ''
                            }
                        </Box>
                        <Box  style={{ padding: '4px' }}  p={1} >

                            {
                                filterEnabled ?
                                <Tooltip title={t('tooltip.filter')}>
                                    <IconButton aria-label="delete" onClick={() => doSearchAndFilter()}>
                                        <SearchIcon/>
                                    </IconButton>
                                </Tooltip>
                                : ''
                            }


                            {
                                pathCreate
                                ?
                                (
                                    <Tooltip title={t('tooltip.create')}>
                                        <IconButton aria-label="delete" href={pathCreate}>
                                            <AddIcon/>
                                        </IconButton>
                                    </Tooltip>
                                ) :
                                ''
                            }

                            {
                                duplicateEnable ?
                                    (
                                        <Tooltip title={ t('tooltip.copy') }>
                                            <IconButton disabled={numSelected <= 0} aria-label="delete" onClick={() => doCopy(null)} >
                                                <FileCopyIcon />
                                            </IconButton>
                                        </Tooltip>
                                    )
                                    : ''
                            }

                            {
                                actionsEnable && pathEditMultiple ?
                                    (
                                        <Tooltip title={t('tooltip.edit_multiple')}>
                                            <IconButton aria-label="delete" onClick={() => doEdit(null)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </Tooltip>
                                    )
                                    :
                                    ''
                            }

                            {
                                deleteEnable
                                    ?
                                    (
                                        <Tooltip title={ t('tooltip.delete') }>
                                            <IconButton onClick={handleDeleteSelected} disabled={numSelected <= 0} aria-label="delete" >
                                                <DeleteIcon />
                                            </IconButton>
                                        </Tooltip>
                                    )
                                    :
                                    ''
                            }
                        </Box>
                    </Box>
                </div>

            }

            {
                error_message !== ''
                    ?
                    <Box style={{ padding: '20px' }} >
                        <Snackbar
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            open={openSnackbar}
                            autoHideDuration={4000}
                            message={error_message}
                        />

                        <Alert severity={error_message_severity}>{error_message}
                            {
                                error_message_severity === 'error'
                                    ?
                                    (
                                        <React.Fragment> - <Link href="/">login</Link></React.Fragment>
                                    )
                                    :
                                    ''
                            }
                        </Alert>
                    </Box>
                    : ''
            }
            <TableContainer >
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell padding="checkbox">
                                {
                                    error_message !== ''
                                    ?
                                    '' :

                                        (actionsEnable ? (<Checkbox
                                        onChange={handleSelectAllClick}
                                        inputProps={{'aria-label': 'select all'}}
                                    />) : '')
                                }
                            </TableCell>
                            {
                                // Header Table Data
                                headerCell.map((row, index) => (



                                        typeof row.hidden !== 'undefined' && row.hidden

                                        ? '' :
                                        (<TableCell key={row.label}>
                                            {

                                                typeof row.sort !== 'undefined' && row.sort
                                                    ?

                                                    (
                                                        <TableSortLabel
                                                            active={true}
                                                            direction={fieldSorted === (typeof row.sortkey !== 'undefined' && row.sortkey !== '' ? row.sortkey : row.key) && sort === 'asc' ? 'desc' : 'asc'}
                                                            onClick={() => doSortBy((typeof row.sortkey !== 'undefined' && row.sortkey !== '' ? row.sortkey : row.key), sort)}
                                                        >{t(row.label)}</TableSortLabel>

                                                    )

                                                    :

                                                    t(row.label)

                                            }

                                        </TableCell>)

                                ))
                            }
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>

                    { isLoading
                        ?
                        <TableBody>
                            <TableRow>
                                <TableCell colSpan={headerCell.length+2} align="center">
                                    <CircularProgress disableShrink />
                                    <br />{ t('text.loading') }
                                </TableCell>
                            </TableRow>
                        </TableBody>
                        :
                        <TableBody>
                            {
                                error_message_severity !== 'error' && rows.length > 0 && rows.map( (row) => {

                                    const isItemSelecdted = isItemSelected(row.id);
                                    
                                    return (
                                        <TableRow key={row.id}
                                                    onClick={actionsEnable ? (event) => handleClick(event, row.id) : null}
                                        >
                                            <TableCell padding="checkbox">
                                                { actionsEnable ? (<Checkbox
                                                    checked={isItemSelecdted}
                                                    inputProps={{'aria-labelledby': 'labelId'}}
                                                />) : '' }
                                            </TableCell>

                                            {
                                                // Body Table Data
                                                headerCell.map((header) => (

                                                    typeof header.hidden !== 'undefined' && header.hidden

                                                        ? '' :
                                                    (<TableCell key={header.key}>
                                                        {Formatter.format(row[header.key], header)}
                                                    </TableCell>)


                                                ))
                                            }

                                            <TableCell >
                                                {
                                                    pathEdit ?
                                                        (<Tooltip title={ t('tooltip.edit') }>
                                                            <IconButton
                                                                aria-label="more"
                                                                aria-controls="long-menu"
                                                                aria-haspopup="true"
                                                                onClick={() => doEdit(row.id)}
                                                            >
                                                                <EditIcon />
                                                            </IconButton>
                                                        </Tooltip>)
                                                        : ''
                                                }


                                                {duplicateEnable ? (<Tooltip title={ t('tooltip.copy') }>
                                                    <IconButton
                                                        aria-label="more"
                                                        aria-controls="long-menu"
                                                        aria-haspopup="true"
                                                        onClick={() => doCopy(row.id)}
                                                    >
                                                        <FileCopyIcon />
                                                    </IconButton>
                                                </Tooltip>) : '' }

                                                { deleteEnable ?
                                                (<Tooltip title={ t('tooltip.delete') }>
                                                    <IconButton
                                                        aria-label="more"
                                                        aria-controls="long-menu"
                                                        aria-haspopup="true"
                                                        onClick={() => handleDeleteOne(row.id)}
                                                    >
                                                        <DeleteIcon />
                                                    </IconButton>
                                                </Tooltip>) : '' }

                                            </TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                        </TableBody>

                    }

                    {
                        error_message_severity === 'error'
                            ?
                            '' :
                            (
                                <React.Fragment>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            rowsPerPageOptions={[30, 5, 10, 25, 50, 100, 200]}
                                            colSpan={headerCell.length + 2}
                                            count={totalItems}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            SelectProps={{
                                                inputProps: {'aria-label': 'rows per page'},
                                                native: true,
                                            }}
                                            onChangePage={handleChangePage}
                                            onChangeRowsPerPage={handleChangeRowsPerPage}
                                            ActionsComponent={TablePaginationActions}
                                        />
                                    </TableRow>
                                </TableFooter>
                                </React.Fragment>
                            )
                    }
                </Table>
            </TableContainer>

            {/* Dialog confirm */}
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{ t('text.dialog_remove_title') }</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        { t('text.dialog_confirm_ask') }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleOkDialog} color="primary">
                        { t('field.button_yes') }
                    </Button>
                    <Button onClick={handleCloseDialog} color="primary" autoFocus>
                        { t('field.button_no') }
                    </Button>
                </DialogActions>
            </Dialog>

            {/* Dialog Filter */}
            <Dialog
                fullWidth
                open={openDialogFilter}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{ t('text.dialog_filter_title') }</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Formik
                            enableReinitialize

                            initialValues={
                                keyList
                            }

                            onSubmit={(values, { setSubmitting }) => {
                                setSubmitting(true);

                                values = prepareFieldsToSave(values);

                                // console.log('fieldSubmit: (keyList) ' + JSON.stringify(keyList));
                                // console.log('fieldSubmit: (values) ' + JSON.stringify(values));return;

                                let filterSearchAdded = [];

                                let listParams = [];
                                let listParamsWithtype = [];
                                for(let obj in values) {




                                    // split obj by "_"
                                    let explodeObj = obj.split('_');
                                    let fieldType = explodeObj[0];
                                    let fieldName = explodeObj[1];
                                    let fieldValue = values[obj];


                                    // headerCell.filter.apiRelation
                                    let apiRelation = '';
                                    let fieldDb = '';
                                    headerCell.map( (row) => {
                                        if (typeof row.filter !== 'undefined' && row.filter && row.key == fieldName) {
                                            apiRelation = row.filter.apiRelation
                                            fieldDb = row.filter.fieldDb
                                            return true;
                                        }
                                    })

                                    // console.log('fieldType: ' + fieldType + ' -  fieldValue: ' + fieldValue + ' - fieldName: ' + fieldName);



                                    // Number
                                    if (fieldType === 'range' && parseInt(fieldValue)>0) {
                                        let fieldOption = `optionrange_${fieldName}`
                                        let valueFieldOption = values[fieldOption]


                                        filterSearchAdded.push({ [`${fieldType}_${fieldName}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldName}=${fieldValue}`)
                                        filterSearchAdded.push({ [`optionrange_${fieldName}`]: valueFieldOption})
                                        listParamsWithtype.push(`optionrange_${fieldType}_${fieldName}=${fieldValue}`)

                                        setFilterSearch(filterSearchAdded );


                                        // ?fieldName[optionFilter]=fieldValue
                                        listParams.push(`${fieldDb}${typeof valueFieldOption === 'undefined' ? '' : `[${valueFieldOption}]`}=${fieldValue}`)
                                    }

                                    // Entity translation
                                    if (fieldType === 'translation' && parseInt(fieldValue)>0) {


                                        filterSearchAdded.push({ [`${fieldType}_${fieldDb}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldDb}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );

                                        // convert value to comprehensive api
                                        fieldValue = strings.sprintf(apiRelation, fieldValue);

                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}=${fieldValue}`)
                                    }

                                    // Native entity
                                    if (fieldType === 'nativeEntity' && parseInt(fieldValue)>0) {

                                        filterSearchAdded.push({ [`${fieldType}_${fieldDb}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldDb}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );

                                        // convert value to comprehensive api
                                        fieldValue = strings.sprintf(apiRelation, fieldValue);

                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}=${fieldValue}`)
                                    }

                                    // Entity relation
                                    if (fieldType === 'relation' && fieldValue !== '') {

                                        filterSearchAdded.push({ [`${fieldType}_${fieldDb}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldDb}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );

                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}=${fieldValue}`)
                                    }

                                    // Date
                                    if (fieldType === 'date' && fieldValue !== '') {
                                        let fieldOption = `optiondate_${fieldName}`
                                        let valueFieldOption = values[fieldOption]

                                        filterSearchAdded.push({ [`${fieldType}_${fieldName}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldName}=${fieldValue}`)
                                        filterSearchAdded.push({ [`optiondate_${fieldName}`]: valueFieldOption})
                                        listParamsWithtype.push(`optiondate_${fieldType}_${fieldName}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );


                                        fieldValue = moment(fieldValue).format('YYYY-MM-DD');

                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}${typeof valueFieldOption === 'undefined' ? '' : `[${valueFieldOption}]`}=${fieldValue}`)
                                    }

                                    // Text
                                    if (fieldType === 'text' && fieldValue !== '') {
                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}=${fieldValue}`)
                                        filterSearchAdded.push({ [`${fieldType}_${fieldDb}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldDb}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );

                                    }

                                    // Autocomplete
                                    if (fieldType === 'autocomplete' && fieldValue !== '') {
                                        // ?fieldName=fieldValue
                                        listParams.push(`${fieldDb}=${fieldValue}`)
                                        filterSearchAdded.push({ [`${fieldType}_${fieldDb}`]: fieldValue})
                                        listParamsWithtype.push(`${fieldType}_${fieldDb}=${fieldValue}`)
                                        setFilterSearch(filterSearchAdded );
                                    }





                                }

                                 console.log('fieldType:  (filterSearchAdded) ' + JSON.stringify(filterSearchAdded));


                                let url = apiUrl + pathApiUrl;
                                // if params > doSearch
                               if (listParams.length>0) {
                                   url += '?' + listParams.join('&');

                                   // Change url
                                   history.push(location.pathname + '?' + listParams.join('&'))

                                   // Keep url in localStorage
                                   SessionService.store("referrer", location.pathname + '?' + listParams.join('&'))
                                   SessionService.store("api_url", url)
                                   SessionService.store("query_string", listParamsWithtype)
                                   SessionService.store("filter_search", JSON.stringify(filterSearchAdded))

                                   // Keep Param
                                   setQueryString(listParams);
                               }


                                fetchUrl(url)

                                // Close Dialog
                                setOpenDialogFilter(false)
                               return;




                            }}

                            validationSchema={Yup.object().shape({})}
                        >

                            {(props) => {
                                const {
                                    handleSubmit
                                } = props;
                                return (
                                    <form onSubmit={handleSubmit}>

                                        {
                                            headerCell.map( (row) => {


                                                if (typeof row.filter !== 'undefined' && row.filter && !['autocomplete'].includes(row.filter.type)) {
                                                    return (
                                                        <React.Fragment>

                                                            <Filterfield filter={filter} t={t} field={row}/> <br/>

                                                        </React.Fragment>
                                                    )
                                                }

                                                // Autocomplete
                                                if (typeof row.filter !== 'undefined' && row.filter && ['autocomplete'].includes(row.filter.type)) {
                                                    return (
                                                        <React.Fragment>

                                                            <AutocompleteField
                                                                placeholder={t(row.label)}
                                                                name={`autocomplete_${row.key}`}
                                                                service={row.filter.service}
                                                                value={filterAutocompleteValue[`autocomplete_${row.key}`]}
                                                                options={[filterAutocompleteValue[`autocomplete_${row.key}`]]}
                                                                onChange={handleChangeReactSelect}

                                                            />
                                                            <br />

                                                        </React.Fragment>
                                                    )
                                                }

                                            }


                                            )
                                        }

                                        <DialogActions>
                                            <Button type="submit" color="primary">
                                                { t('field.button_search_filter') }
                                            </Button>
                                            <Button onClick={handleResetFilter} color="primary" autoFocus>
                                                { t('field.button_search_reset_filter') }
                                            </Button>
                                            <Button onClick={() => handleCloseDialogFilter()} color="primary" autoFocus>
                                                { t('field.button_close_dialog') }
                                            </Button>
                                        </DialogActions>

                                    </form>
                                );
                            }}
                        </Formik>

                    </DialogContentText>
                </DialogContent>
            </Dialog>


        </React.Fragment>


    )
}

export default DataTableRemote;
