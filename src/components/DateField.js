import React from 'react';
import Typography from '@material-ui/core/Typography';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {Field} from "formik";

const DatePickerField = ({ field, form, ...other }) => {

    const currentError = form.errors[field.name];

    return (
        <React.Fragment>
            <Typography>{ other.labelkeytranslate }</Typography>
            {/*{JSON.stringify(field)}*/}
            <KeyboardDatePicker
                fullWidth={other.fullWidth}
                autoOk={other.autoOk}
                value={field.value}
                variant={other.variant}
                format={other.format}
                margin={other.margin}
                name={field.name}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                helperText={currentError}
                error={Boolean(currentError)}
                onError={error => {
                    // handle as a side effect
                    if (error !== currentError) {
                        form.setFieldError(field.name, error);
                    }
                }}
                // if you are using custom validation schema you probably want to pass `true` as third argument
                onChange={date => form.setFieldValue(field.name, date, false)}
                onAccept={other.onAccept}
                {...other}
            />
        </React.Fragment>
    );
};

const DateField = (props) => {
    return (
        <React.Fragment>

            {/*{JSON.stringify(props)}*/}

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Field
                    variant={props.variant}
                    labelkeytranslate={props.labelkeytranslate}
                    format={props.format}
                    name={props.name}
                    autoOk={props.autoOk}
                    fullWidth={props.fullWidth}
                    disabled={props.disabled}
                    onAccept={props.onAccept}
                    component={DatePickerField} />
            </MuiPickersUtilsProvider>

        </React.Fragment>
    )
}



export default DateField;
