import React from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';


const Copyright = () => {
  return (
      <Typography variant="body2" color="textSecondary" align="center">
          <Link target="_blank" color="inherit" href="https://covid19mg.madatsara.com">
              <strong>covid19mg.madatsara.com</strong>
          </Link> -
      {'Conception et UX (Andy) '}
      {'.'} - <Link target="_blank" color="inherit" href="https://miary.dev">
          Dev (Miary)
          </Link>
      {' - '} Hosted by <Link target="_blank" color="inherit" href="https://madatsara.com">
          Madatsara
          </Link>{' '}
      {new Date().getFullYear()}
      {' - powered by reactjs'}
    </Typography>
  )
}

export default Copyright;
