import React, {useState} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from "@material-ui/core/Tooltip";
import ListSubheader from "@material-ui/core/ListSubheader";
import LockOpenIcon from '@material-ui/icons/LockOpen';
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TranslateIcon from '@material-ui/icons/Translate';
import Collapse from "@material-ui/core/Collapse";
import {ExpandLess, ExpandMore } from "@material-ui/icons";

// react-router
import {
    BrowserRouter as Router, Link,
    Route,
    Switch,
    useHistory
} from 'react-router-dom'

import { SidebarMenu } from './components/listItems'

import AuthService from "./service/AuthService";

// Footer
import Copyright from "./components/Copyright";

// Routes
import RouteBackend from "./utils/RouteBackend";
import RouteFrontend from "./utils/RouteFrontend";

// react-i18next
import {useTranslation} from "react-i18next";


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    grow: {
        flexGrow: 1,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));



const DefaultContainer = () => {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [openDialog, setOpenDialog] =  useState(false);
    const history = useHistory();
    const { t, i18n } = useTranslation();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [collapsed, setCollapsed] = React.useState([]);



    const handleProfileMenuOpen = (event) => {
        setOpenDialog(true)
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const handleLogout = () => {
        setOpenDialog(false);
        AuthService.logOut()
        history.push('/')
    };


    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
        handleClose();
    };

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const menuId = 'primary-search-account-menu';


    const handleOpenCloseCollapse = (e) => {

        // Add in state if not exists
        if (!collapsed.includes(e)) {
            setCollapsed(collapsed => [...collapsed, e]);
        }
        // Remove from state if exists
        if (collapsed.includes(e)) {
            let _collapsed = collapsed.filter(item => item !== e);
            setCollapsed(collapsed => _collapsed);
        }
    };

    return (
        <React.Fragment><div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        {t('text.title')}
                    </Typography>
                    <div className={classes.grow} />
                    <div>
                        <Tooltip title={t('tooltip.logout')}>
                            <IconButton
                            aria-label="account of current user"
                            aria-haspopup="true"
                            color="inherit"
                            aria-controls={menuId}
                            edge="end"
                            onClick={handleProfileMenuOpen}
                        >
                            <LockOpenIcon />
                            </IconButton>
                        </Tooltip>

                    </div>
                </Toolbar>
            </AppBar>
            <Router>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>

                    <Button
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                        variant="outlined"
                        startIcon={<TranslateIcon />}
                    >
                        { t('field.button_switch_lang')}
                    </Button>
                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <MenuItem onClick={() => { changeLanguage('fr') }}>FR</MenuItem>
                        <MenuItem onClick={() => { changeLanguage('mg') }}>MG</MenuItem>
                    </Menu>

                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                {
                    SidebarMenu.map((v,i) =>
                        (
                            <React.Fragment>
                                <List>
                                    { v.label !== '' ? <ListSubheader inset>{ t('sidebar_menu.' + v.key) }</ListSubheader> : '' }
                                    {
                                        v.items.map((row,i) => (
                                            <React.Fragment>
                                                <ListItem button key={row.id} component={Link} to={row.to}  onClick={ handleOpenCloseCollapse.bind(this,row.id) }>
                                                    <ListItemIcon>{row.icon}</ListItemIcon>
                                                    <ListItemText primary={ t('sidebar_menu.' + row.label)  } />
                                                    { row.items ?
                                                        collapsed.includes(row.id) ? <ExpandLess/> : <ExpandMore/>
                                                     : ''
                                                    }
                                                </ListItem>
                                                { row.items ?
                                                    <Collapse in={collapsed.includes(row.id)} timeout="auto" unmountOnExit>
                                                        <List component="div" disablePadding>
                                                            {
                                                                row.items.map((row,i) => (
                                                                    <ListItem component={Link} to={row.to} button className={classes.nested}>
                                                                        <ListItemIcon>{row.icon}</ListItemIcon>
                                                                        <ListItemText primary={ t('sidebar_menu.' + row.label)  } />
                                                                    </ListItem>
                                                                ))
                                                            }

                                                        </List>
                                                    </Collapse>
                                                    :
                                                    ''
                                                }
                                            </React.Fragment>
                                        ))
                                    }
                                </List>
                                <Divider />
                            </React.Fragment>
                        )
                    )
                }
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >

                <div className={classes.drawerHeader} />
                <RouteBackend />

            </main>

            </Router>

            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{t('text.dialog_logout_title')}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {t('text.dialog_confirm_ask')}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleLogout} color="primary">
                        {t('field.button_yes')}
                    </Button>
                    <Button onClick={handleCloseDialog} color="primary" autoFocus>
                        {t('field.button_no')}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
            <Copyright />
        </React.Fragment>
    );
}

const App = () => {
    return (
        <React.Fragment>
            <React.Suspense fallback={(<div>Loading</div>)} >
                <Router>
                    <Switch>
                        <Route exact path="/" component={RouteFrontend}/>
                        <Route  render={(props) => <DefaultContainer />}/>
                    </Switch>
                </Router>
            </React.Suspense>
        </React.Fragment>
    )
}

export default App
