
class SessionService {

    getUserInfo(){
        let storageContent = this.get("userInfo");

        if (storageContent === '') {
            return;
        }

        return JSON.parse(storageContent);
    }

    getRememberMe(){
        let storageContent = this.get("userInfo");
        if (storageContent === null) {
            return;
        }

        let data = JSON.parse(storageContent);

        return data.rememberme
    }

    getAuthHeader() {
        if (!this.getUserInfo()) {
            return;
        }

        return {headers: {'Content-Type': 'application/json', Authorization: 'Bearer ' + this.getUserInfo().token }};
    }

    store(key, value) {
        localStorage.setItem(key, value);
    }

    get(key) {
        let storageContent = localStorage.getItem(key) !== null ? localStorage.getItem(key) : '';
        return storageContent;
    }

    removeAll(keys) {
        keys.map( (key) => {
            this.remove(key)
        })
    }

    remove(key) {
        localStorage.removeItem(key)
    }
}

export default new SessionService();
