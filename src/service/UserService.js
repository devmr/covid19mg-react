import axios from 'axios';
import SessionService from './SessionService';

const USER_API_BASE_URL = 'http://localhost:8080/users';

class UserService {

    fetchUsers() {
        return axios.get(USER_API_BASE_URL, SessionService.getAuthHeader());
    }

    fetchUserById(userId) {
        return axios.get(USER_API_BASE_URL + '/' + userId, SessionService.getAuthHeader());
    }

    deleteUser(userId) {
        return axios.delete(USER_API_BASE_URL + '/' + userId, SessionService.getAuthHeader());
    }

    addUser(user) {
        return axios.post(""+USER_API_BASE_URL, user, SessionService.getAuthHeader());
    }

    editUser(user) {
        return axios.put(USER_API_BASE_URL + '/' + user.id, user, SessionService.getAuthHeader());
    }

}

export default new UserService();
