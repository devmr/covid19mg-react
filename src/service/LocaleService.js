import axios from 'axios';
import { Config } from "../utils/config";
import SessionService from "./SessionService";

class LocaleService {

    findAll() {
        return axios.get(Config.host() + '/api/locale', SessionService.getAuthHeader());
    }


}

export default new LocaleService();
