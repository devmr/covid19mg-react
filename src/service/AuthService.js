import axios from 'axios';
import { Config } from "../utils/config";

const USER_API_BASE_URL = Config.host();

class AuthService {

    login(credentials){
        return axios.post(USER_API_BASE_URL + "/api/authentication_token", credentials);
    }

    getUserInfo(){
        let storageContent = localStorage.getItem("userInfo") !== null ? localStorage.getItem("userInfo") : '{"token":""}';
        return JSON.parse(storageContent);
    }

    getRememberMe(){
        let storageContent = localStorage.getItem("userInfo") !== null ? localStorage.getItem("userInfo") : null;
        if (storageContent === null) {
            return;
        }

        let data = JSON.parse(storageContent);

        return data.rememberme
    }

    getAuthHeader() {
        return {headers: {'Content-Type': 'application/json', Authorization: 'Bearer ' + this.getUserInfo().token }};
    }

    logOut() {
        localStorage.removeItem("userInfo");
    }

    refreshToken(token){
        return axios.post(USER_API_BASE_URL + "/api/refresh_token", token);
    }
}

export default new AuthService();
