import axios from 'axios';
import { Config } from "../utils/config";
import SessionService from "./SessionService";

const BASE_URL = Config.host() + '/api/translations';

class TranslationService {

    findAll() {
        return axios.get(BASE_URL, SessionService.getAuthHeader());
    }

    getAllLang() {
        return axios.get(Config.host() + '/api/locale', SessionService.getAuthHeader());
    }

    findById(id) {
        return axios.get(BASE_URL + '/' + id, SessionService.getAuthHeader());
    }

    findByMultipleIds(ids) {
        return axios.get(BASE_URL + '/edit/' + ids, SessionService.getAuthHeader());
    }

    remove(id) {
        return axios.delete(BASE_URL + '/' +  id, SessionService.getAuthHeader());
    }

    create(entity) {
        return axios.post(BASE_URL, entity, SessionService.getAuthHeader());
    }

    update(entity, id) {
        return axios.put(BASE_URL + '/' + id, entity, SessionService.getAuthHeader());
    }

    duplicate(id) {
        return axios.get(BASE_URL + '/' +  id + '/duplicate', SessionService.getAuthHeader());
    }

    filterBy(s,field) {
        return axios.get(BASE_URL + '?' + field + '=' + s, SessionService.getAuthHeader());
    }

    updateAll(entity) {
        return axios.post(BASE_URL + '/update-all', entity, SessionService.getAuthHeader());
    }


}

export default new TranslationService();
