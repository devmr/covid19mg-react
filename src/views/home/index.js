import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import UpdateIcon from '@material-ui/icons/Update';

import Chart from "react-apexcharts";
import {withTranslation} from "react-i18next";

import DataDateTypeService from "../../service/DataDateTypeService";
import DataProvincesService from "../../service/DataProvincesService";
import DataTypesService from "../../service/DataTypesService";
import AuthService from "../../service/AuthService";

import {Formatter} from "../../utils/formatter";
import Divider from "@material-ui/core/Divider";
import Moment from "react-moment";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import {withStyles} from "@material-ui/core";
import SessionService from "../../service/SessionService";
import Skeleton from "@material-ui/lab/Skeleton";



class Dashboard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            loadingDataDateTypes: true,
            loadingDataProvinces: true,
            loadingDataTypes: true,

            textCasesConfirmed: '',
            numberCasesConfirmed: '',
            textCasesRecovered: '',
            numberCasesRecovered: '',
            textCasesDeath: '',
            numberCasesDeath: '',
            updatedAt: null,

            chartLineSeries: [],
            chartBarSeries: [],
            chartLineOptions: {
                    chart: {
                        height: 350,
                        type: 'line',
                        zoom: {
                            enabled: false
                        },
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        width: [],
                        curve: 'straight',
                        dashArray: []
                    },
                    title: {
                        text: '',
                        align: 'left'
                    },
                    legend: {
                        tooltipHoverFormatter: function(val, opts) {
                            return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
                        }
                    },
                    markers: {
                        size: 0,
                        hover: {
                            sizeOffset: 6
                        }
                    },
                    xaxis: {
                        categories: [],
                    },
                    tooltip: {
                        y: []
                    },
                    grid: {
                        borderColor: '#f1f1f1',
                    }
                },
            chartBarOptions: {
                chart: {
                    type: 'bar',
                    height: 350
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                    },
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    show: true,
                    width: 2,
                    colors: ['transparent']
                },
                xaxis: {
                    categories: [],
                },
                yaxis: {
                    title: {
                        text: ''
                    }
                },
                fill: {
                    opacity: 1
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                            return  val
                        }
                    }
                }
            },


        };
    }

    findCategoryById(id, dataCategory) {

        id = parseInt(id);
        let value = '';

        dataCategory.map((row) => {
            for (let obj in row) {
                if (obj === 'id' && row[obj] === id) {
                    value = row['label'];
                }
            }
        })

        return value;

    }

    getSettingOption(dataSettings, option) {

        let data = [];

        dataSettings.map((row) => {

            let parsingSetting = JSON.parse(row);


            switch (option) {
                case 'charts.stroke.dash':
                    if (parsingSetting['charts']['stroke']['dash'] !== null) {
                        data.push(parsingSetting['charts']['stroke']['dash'])
                    }
                    break;

                case 'charts.stroke.width':
                    if (parsingSetting['charts']['stroke']['width'] !== null) {
                        data.push(parsingSetting['charts']['stroke']['width'])
                    }
                    break;



            }


        })

        return data

    }

    getIdCases(result) {
        // Each Data
        let tmpcasesIdList = Object.keys(result['hydra:member']['data']);

        // Convert each value in array to int
        let casesIdList = tmpcasesIdList.map((row) => {
            return parseInt(row);
        })

        return casesIdList;
    }

    getDataChartLineLabels(result) {

        let casesIdList = this.getIdCases(result)

        // All Categories -> keys first values
        return Object.keys(result['hydra:member']['data'][casesIdList[0]]);
    }

    geSettingsChartLineSeries(result) {

        let casesIdList = this.getIdCases(result)

        let dataSettings = [];
        result['hydra:member']['entities'].map( (row, i) => {
            let id = row['id'];
            let setting = row['settings'];

            // only if id in key casesIdList
            if (casesIdList.includes(id)) {
                dataSettings.push(setting);
            }
        })

        return dataSettings;


    }

    getDataChartLineSeries(result) {

        let casesIdList = this.getIdCases(result)

        let dataCategory = [];
        result['hydra:member']['entities'].map( (row, i) => {
            let id = row['id'];
            let plainValue =  Formatter.format(row, {options: {type: 'translation' }} );
            let value = plainValue['props']['children'];

            // only if id in key casesIdList
            if (casesIdList.includes(id)) {
                dataCategory.push({id: id, label: value});
            }
        })

        let series = [];
        let dataRows = result['hydra:member'] ;
        for (let obj in dataRows['data']) {
            series.push({name: this.findCategoryById(obj, dataCategory), data: Object.values(dataRows['data'][obj])})
        }

        return series;


    }
    getDataChartBar(result) {


        let dataNumber = [];
        let dataLabels = [];
        result['hydra:member'].map( (row, i) => {
            let number = parseInt(row['number']);
            let name =  row['province']['name'];
            dataNumber.push(number);
            dataLabels.push(name);
        })





        return [{data: dataNumber},dataLabels];
    }

    componentDidMount() {

        // DataDateTypeService
        DataDateTypeService.getDataChartCases().then(res => {


            if (res.status === 200) {

                    let result = res.data;

                    if (result['hydra:member'] === null) {
                        return;
                    }

                    let series = this.getDataChartLineSeries(result)
                    let labelsList = this.getDataChartLineLabels(result)
                    let settingsList = this.geSettingsChartLineSeries(result)

                    this.setState({loadingDataDateTypes: false});
                    this.setState({chartLineSeries: series});
                    this.setState({chartLineOptions:{ xaxis: { categories: labelsList}}});
                    this.setState({chartLineOptions:{ stroke: { width: this.getSettingOption(settingsList, 'charts.stroke.width') }}});
                    this.setState({chartLineOptions:{ stroke: { dashArray: this.getSettingOption(settingsList, 'charts.stroke.dash') }}});

                    // Set States
                    return;


                }

            });


        // DataProvincesService
        DataProvincesService.findAll().then(res => {


            if (res.status === 200) {

                let result = res.data;

                if (result['hydra:member'] === null) {
                    return;
                }

                let dataChartBar = this.getDataChartBar(result)


                let data = dataChartBar[0];
                let dataCateg = dataChartBar[1];


                this.setState({loadingDataProvinces: false});
                this.setState({chartBarSeries: [data]});
                this.setState({chartBarOptions:{ xaxis: { categories: dataCateg}}});


                // Set States
                return;


            }

        });

        DataTypesService.findAll().then(res => {
            if (res.status === 200) {

                let result = res.data;

                if (result['hydra:member'] === null) {
                    return;
                }

                let row = result['hydra:member'];
                if (row.length === 0) {
                    return;
                }


                this.setState({loadingDataTypes: false});

                if (row[0] !== null) {

                    let textCasesConfirmed =  Formatter.format(row[0]['typeCase'], {options: {type: 'translation' }} );
                    textCasesConfirmed = textCasesConfirmed['props']['children'];

                    let numberCasesConfirmed = row[0]['number']
                    let updatedAt = row[0]['updatedAt']

                        this.setState({textCasesConfirmed: textCasesConfirmed});
                    this.setState({numberCasesConfirmed: numberCasesConfirmed});
                    this.setState({updatedAt: updatedAt});
                }

                if (row[1] !== null) {

                    let textCasesRecovered =  Formatter.format(row[1]['typeCase'], {options: {type: 'translation' }} );
                    textCasesRecovered = textCasesRecovered['props']['children'];

                    let numberCasesRecovered = row[1]['number']

                    this.setState({textCasesRecovered: textCasesRecovered});
                    this.setState({numberCasesRecovered: numberCasesRecovered});
                }

                if (row[2] !== null) {

                    let textCasesDeath =  Formatter.format(row[2]['typeCase'], {options: {type: 'translation' }} );
                    textCasesDeath = textCasesDeath['props']['children'];

                    let numberCasesDeath = row[2]['number']

                    this.setState({textCasesDeath: textCasesDeath});
                    this.setState({numberCasesDeath: numberCasesDeath});
                }


            }
            return;
        });


        // if remember me -> keep logged in
        if (AuthService.getRememberMe()) {
            let info = JSON.parse(localStorage.getItem("userInfo"));

            if (info===null) {
                return;
            }

            let data = { refresh_token: info.refresh_token};
            AuthService.refreshToken(data).then(res => {
                if(res.status === 200){
                    // keep remember be
                    if (AuthService.getRememberMe()) {
                        res.data['rememberme'] = true;
                    }

                    SessionService.store("userInfo", JSON.stringify(res.data));
                }
                return;
            });
        }


            return;

    }

    render() {

        const { t, history, classes } = this.props;

        return(
            <React.Fragment>


                    <Grid container spacing={3}>
                        {/* Data by dates */}
                        <Grid item xs={12} md={9}>
                            <Typography variant="h5" align="center">{t('text.title_data_date_type')}</Typography>

                            {
                                this.state.loadingDataDateTypes ?
                                    <Skeleton animation="wave"  style={{ marginTop: 10 }} variant="rect" height={118} />
                                    :
                                    <Chart
                                        options={this.state.chartLineOptions}
                                        series={this.state.chartLineSeries}
                                        type="line"
                                        height={350}
                                    />
                            }
                        </Grid>
                        <Grid item xs={12} md={3}>

                            {/* Global cases */}
                            <Typography variant="h5" align="center">{t('text.title_casetypes')}</Typography>
                            <br />

                            <Paper elevation={4} >
                                <Card>
                                    <CardContent>
                                        {
                                            this.state.loadingDataTypes
                                                ?
                                                (
                                                    <React.Fragment>
                                                        <Skeleton variant="text" height={20} />
                                                        <Skeleton  width={180} height={50} />
                                                        <Divider/>
                                                        <Skeleton variant="text" height={20} />
                                                        <Skeleton  width={100} height={50} />
                                                        <Divider/>
                                                        <Skeleton variant="text" height={20} />
                                                        <Skeleton  width={100} height={50} />
                                                        <Divider/>
                                                    </React.Fragment>
                                                )
                                                :
                                                (
                                                    <React.Fragment>
                                                        <Typography color="textSecondary" gutterBottom>
                                                            {this.state.textCasesConfirmed}
                                                        </Typography>
                                                        <Typography variant="h2" component="h2" align="center">
                                                            {this.state.numberCasesConfirmed}
                                                        </Typography>

                                                        <Divider/>
                                                        <Typography color="textSecondary" gutterBottom>
                                                            {this.state.textCasesRecovered}
                                                        </Typography>
                                                        <Typography variant="h4" component="h2" align="center">
                                                            {this.state.numberCasesRecovered}
                                                        </Typography>

                                                        <Divider/>
                                                        <Typography color="textSecondary" gutterBottom>
                                                            {this.state.textCasesDeath}
                                                        </Typography>
                                                        <Typography variant="h4" component="h2" align="center">
                                                            {this.state.numberCasesDeath}
                                                        </Typography>


                                                        <Typography paragraph align="right">
                                                            <br/>{t('field.updatedAt')} : <strong><Moment
                                                            format="DD-MM-YYYY HH:mm">{this.state.updatedAt}</Moment></strong>
                                                        </Typography>
                                                    </React.Fragment>
                                                )
                                        }


                                    </CardContent>
                                    <CardActions>
                                        <div  className={classes.textCenter}>
                                            <Button
                                                variant="contained"
                                                color="default"
                                                size="large"
                                                disabled={this.state.loadingDataTypes}
                                                onClick={() => {// Go to all rows list
                                                    this.props.history.push('/data_types')
                                                }}
                                                startIcon={<UpdateIcon />}
                                            >
                                                {t('field.button_save_update')}
                                            </Button>
                                        </div>
                                    </CardActions>
                                </Card>
                            </Paper>


                        </Grid>
                    </Grid>
                    <Grid container spacing={12} >
                        <Grid item xs={12} lg={9}>

                            {/* Data by provinces */}
                            <Typography variant="h5" align="center">{t('text.title_data_province')}</Typography>
                            {
                                this.state.loadingDataProvinces ?
                                    <Skeleton animation="wave" style={{marginTop: 10}} variant="rect" height={118}/>
                                    :
                                    <Chart
                                        options={this.state.chartBarOptions}
                                        series={this.state.chartBarSeries}
                                        type="bar"
                                        height={350}
                                    />
                            }

                        </Grid>
                        <Grid item xs={12} lg={3}>
                            <br /><br />
                            <Typography variant="h6" align="center">{t('text.title_data_province')}</Typography>
                            <br />

                            <div className={classes.textCenter}>
                                <Button
                                    variant="contained"
                                    color="default"
                                    size="large"
                                    disabled={this.state.loadingDataTypes}
                                    onClick={() => {
                                        this.props.history.push('/data_provinces')
                                    }}
                                    startIcon={<UpdateIcon />}
                                >
                                    {t('field.button_save_update')}
                                </Button>
                            </div>

                            <br />
                            <Divider />

                            <Typography variant="h6" align="center">{t('text.title_data_regions')}</Typography>
                            <br /><br />

                            <div className={classes.textCenter}>
                                <Button
                                    variant="contained"
                                    color="default"
                                    size="large"
                                    disabled={this.state.loadingDataTypes}
                                    onClick={() => {// Go to all rows list
                                        this.props.history.push('/data_regions')
                                    }}
                                    startIcon={<UpdateIcon />}
                                >
                                    {t('field.button_save_update')}
                                </Button>
                            </div>

                            <br />
                            <Divider />

                            <Typography variant="h6" align="center">{t('text.title_data_districts')}</Typography>
                            <br /><br />

                            <div className={classes.textCenter}>
                                <Button
                                    variant="contained"
                                    color="default"
                                    size="large"
                                    disabled={this.state.loadingDataTypes}
                                    onClick={() => {
                                        this.props.history.push('/data_districts')
                                    }}
                                    startIcon={<UpdateIcon />}
                                >
                                    {t('field.button_save_update')}
                                </Button>
                            </div>



                        </Grid>
                    </Grid>





            </React.Fragment>
        )
    }



}

const useStyles = theme => ({
    textCenter: {
        textAlign: 'center'
    }
});

// export default withTranslation()(Dashboard);
export default withTranslation()(withStyles(useStyles)(Dashboard));
