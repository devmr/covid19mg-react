import Model from "../../entity/data_regions";

import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from "@material-ui/core/Container";

import {withTranslation} from "react-i18next";

import Form from "../../components/Form";

const keyCreate = 'text.title_create';
const keyEntity = 'text.title_data_regions';

const keySuccessSubmitted = 'text.success_created';
const keyButtonSave = 'field.button_save';

class Create extends React.Component {

    render() {

        const { t, history } = this.props;

        return(
            <React.Fragment>

                <Container maxWidth="sm">

                    <div >
                        <Typography variant="h5" align="center">{t(keyCreate)} - {t(keyEntity)}</Typography>
                    </div>

                    <Form
                        model={Model}
                        keySuccessSubmitted={keySuccessSubmitted}
                        keyButtonSave={keyButtonSave}
                        history={history}
                    />

                </Container>
            </React.Fragment>
        )
    }
}

export default withTranslation()(Create);
