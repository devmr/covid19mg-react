import Model from "../../entity/data_regions";

import React from 'react';
import Typography from "@material-ui/core/Typography";
import DataTableRemote from "../../components/DataTableRemote";
import {useTranslation} from "react-i18next";


const actions = Model.list.actions;
const titlePage = Model.list.titlePage;
const headerCell = Model.list.headerCell;
const Service = Model.service;

const Entity = () => {
    const { t } = useTranslation();

    return (
        <React.Fragment>
            <Typography variant="h3" align="center">{ t(titlePage) }</Typography>
            <DataTableRemote
                headerCell={headerCell}
                service={Service}
                actions={actions}
            />
        </React.Fragment>
    )
}

export default Entity;
