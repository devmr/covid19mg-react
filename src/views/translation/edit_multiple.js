import Model from "../../entity/translation";

import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from "@material-ui/core/Container";

import {withTranslation} from "react-i18next";

import Form from "../../components/Form";

const keyTitlePage = 'text.title_edit_multiple';
const keySuccessSubmitted = 'text.success_updated';
const keyButtonSave = 'field.button_save_update_all';

class EditMultiple extends React.Component {

    render() {

        const { t, history, match  } = this.props;

        return(
            <React.Fragment>

                <Container>

                    <div >
                        <Typography variant="h5" align="center">{ t(keyTitlePage) }</Typography>
                    </div>

                    <Form
                        model={Model}
                        keySuccessSubmitted={keySuccessSubmitted}
                        keyButtonSave={keyButtonSave}
                        history={history}
                        match={match}
                    />

                </Container>
            </React.Fragment>
        )
    }
}

export default withTranslation()(EditMultiple);
