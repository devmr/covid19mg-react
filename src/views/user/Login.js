import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Alert from "@material-ui/lab/Alert";
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthService from '../../service/AuthService';
import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
// Icon Show password
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from "@material-ui/core/IconButton";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Tooltip from "@material-ui/core/Tooltip";

// Formik and Yup
import {
    Formik
} from 'formik';
import * as Yup from 'yup';

// i18next
import { withTranslation  } from 'react-i18next';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Switch from "@material-ui/core/Switch/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import SessionService from "../../service/SessionService";

class Login extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            message: '',
            messageSeverity: '',
            isSubmitted: false,
            showPassword: false,
        }
        this.login = this.login.bind(this);
    }

    componentDidMount() {
        //localStorage.clear();
        let info = JSON.parse(localStorage.getItem("userInfo"));

        if (info !== null) {
            this.props.history.push('/home');
            return;
        }

        return;

    }

    resetStateError = () => {
        this.setState({message: ""});
        this.setState({messageSeverity: ""});
    }

    flashMessage = (severity, message) => {
        this.setState({message: message});
        this.setState({messageSeverity: severity});
    }

    handleClickShowPassword = () => {
        this.setState({showPassword: !this.state.showPassword });
    };

    handleMouseDownPassword = (e) => {
        e.preventDefault();
    };

    login = (formValue) => {

        const { t } = this.props;

        // console.log(formValue.email);
        // console.log(formValue.password);

        this.resetStateError()
        this.setState({ isSubmitted: true })


        const credentials = { email: formValue.email, password: formValue.password};
        AuthService.login(credentials).then(res => {
            if(res.status === 200){

                // keep remember be
                if (formValue.rememberme) {
                    res.data['rememberme'] = true;
                }

                SessionService.store("userInfo", JSON.stringify(res.data));

                this.flashMessage("success", t('text.success_loggedin'))


                setTimeout( () => {
                    this.props.history.push('/home');
                }, 2000)
            }else {
                this.flashMessage("error", res.data.message)
            }
        }).catch((error) => {
            if (error.response) {
                this.setState({ isSubmitted: false })
                this.flashMessage("error", error.response.data.message)
            } else if (error.request) {
                console.log(error.request);
            } else {
                this.setState({ isSubmitted: false })
                // Something happened in setting up the request and triggered an Error
                this.flashMessage("error", error.message)
            }
        });
    };

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });



    render() {
        const { t, classes } = this.props;

        return(
            <React.Fragment>
                <Grid container component="main" className={classes.root} >
                    <CssBaseline />
                    <Grid item xs={false} sm={4} md={7} className={classes.image}>


                        <div className={classes.box}>

                            <div style={styles.center}>
                                <a target="_blank" href="https://covid19mg.madatsara.com"  rel="noopener noreferrer" >
                                    <img src={require(`../../img/logo.svg`)} alt="" />
                                </a>
                            </div>

                            <Typography component="h1" variant="h3" >
                            { t('text.title') }
                            </Typography>
                            <ul>
                                <li className={classes.ulli}>Powered with <span role="img" aria-label="love">❤️</span> by Andy & <a rel="noopener noreferrer" target="_blank" href="https://miary.dev" className={classes.colorwhite}>Miary</a></li>
                                <li className={classes.ulli}>Built with reactjs & Symfony</li>
                                <li className={classes.ulli}>Free hosting on <a rel="noopener noreferrer" target="_blank" href="https://madatsara.com" className={classes.colorwhite}>madatsara.com</a></li>
                                <li className={classes.ulli}><a rel="noopener noreferrer" target="_blank" href="https://covid19mg.madatsara.com" className={classes.colorwhite}>Covid19mg website</a></li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square style={styles.boxCenter}>


                            <div className={classes.paper}>
                                <Avatar>
                                    <LockOutlinedIcon />
                                </Avatar>


                                 <Formik
                                    initialValues={
                                        {
                                            email: null,
                                            password: null,
                                            rememberme: null
                                        }
                                    }
                                    onSubmit={(values, { setSubmitting }) => {
                                        setSubmitting(true);
                                        this.login(values)

                                    }}

                                    validationSchema={Yup.object().shape({
                                        email: Yup.string()
                                            .email(t('error.email_incorrect'))
                                            .required(t('error.field_empty')),
                                        password: Yup.string()
                                            .required(t('error.field_empty')),
                                    })}
                                >
                                    {(props) => {
                                        const {
                                            values,
                                            touched,
                                            errors,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit
                                        } = props;
                                        return (
                                <form onSubmit={handleSubmit}>


                                    {
                                        this.state.message !== ''
                                        ?
                                        <Alert severity={this.state.messageSeverity}>{this.state.message}</Alert>
                                        : ''
                                    }
                                    <TextField
                                        autoFocus={true}
                                        variant="outlined"
                                        type="text"
                                        label={t('field.username')}
                                        fullWidth
                                        margin="normal"
                                        name="email"
                                        disabled={this.state.isSubmitted}
                                        value={values.email}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        helperText={(errors.email && touched.email) && errors.email}
                                    />

                                    <FormControl fullWidth margin="normal" variant="outlined">
                                        <InputLabel htmlFor="outlined-adornment-password">{t('field.password')}</InputLabel>
                                        <OutlinedInput
                                            disabled={this.state.isSubmitted}
                                            id="outlined-adornment-password"
                                            type={this.state.showPassword ? 'text' : 'password'}

                                            name="password"
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={this.handleClickShowPassword}
                                                        onMouseDown={this.handleMouseDownPassword}
                                                        edge="end"
                                                    >
                                                        {
                                                            this.state.showPassword ?
                                                            (
                                                                <Tooltip title={t('tooltip.hide_password')}><Visibility /></Tooltip>
                                                            ) :
                                                            (
                                                                <Tooltip title={t('tooltip.show_password')}><VisibilityOff /></Tooltip>
                                                            )
                                                        }
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                            labelWidth={70}

                                            value={values.password}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={(errors.password && touched.password) && errors.password}
                                        />
                                        <p className="MuiFormHelperText-root MuiFormHelperText-contained">
                                            {errors.password && touched.password && errors.password}
                                        </p>
                                    </FormControl>

                                    <FormControlLabel
                                        control={<Switch
                                            onChange={handleChange}
                                            name="rememberme" />}
                                        label={t('field.remember_me')}
                                    />


                                    {
                                        this.state.isSubmitted
                                            ?
                                            <LinearProgress/> : ''
                                    }

                                    <br />
                                    <Button
                                        fullWidth
                                        disabled={this.state.isSubmitted}
                                        type="submit"
                                        variant="contained"
                                        color="secondary" >
                                        {
                                            this.state.isSubmitted
                                            ?
                                                t('text.loading')
                                                :
                                                t('field.button_login')
                                        }
                                    </Button>

                                </form>
                                        );
                                    }}
                                </Formik>
                            </div>
                    </Grid>
                </Grid>
            </React.Fragment>
        )
    }

}

const styles= {
    center :{
        display: 'flex',
        justifyContent: 'center',
        margin: '1rem 0'

    },
    notification: {
        display: 'flex',
        justifyContent: 'center',
        color: '#dc3545'
    },

    boxCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    }
}

const useStyles = theme => ({
    root: {
        height: '100vh',
    },
    image: {
        // backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor: '#9b2c2c',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    box: {
        color: '#ffffff',
        padding: '2rem',

    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    colorwhite: {
        color: '#ffffff',
    },
    ulli: {
        margin: '1rem'
    }
});

export default withTranslation()(withStyles(useStyles)(Login));
