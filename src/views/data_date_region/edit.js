import Model from "../../entity/data_date_region";

import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from "@material-ui/core/Container";

import {withTranslation} from "react-i18next";

import Form from "../../components/Form";

const keyEdit = 'text.title_edit';
const keyEntity = 'text.title_data_date_regions';

const keySuccessSubmitted = 'text.success_updated';
const keyButtonSave = 'field.button_save_update';

class Edit extends React.Component {

    render() {

        const { t, history, match  } = this.props;

        return(
            <React.Fragment>

                <Container maxWidth="sm">

                    <div >
                        <Typography variant="h5" align="center">{ t(keyEdit) } - {t(keyEntity)}</Typography>
                    </div>

                    <Form
                        model={Model}
                        keySuccessSubmitted={keySuccessSubmitted}
                        keyButtonSave={keyButtonSave}
                        history={history}
                        match={match}
                    />

                </Container>
            </React.Fragment>
        )
    }
}

export default withTranslation()(Edit);
