import Service from "../service/DataSexeService";
import SexesService from "../service/SexesService";


const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_data_sexe',
        actions: {
            'pathlist': '/data_sexes', // URL to API https://
            'api': '/api/data_sexes', // URL to API https://
            'edit': '/edit-data_sexes/', // Edit mode
            'editmultiple': false, // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': false, // Delete mode
            'create': false, // Create mode
        },
        headerCell: [
            {
                'label':'entity.sex.header.field_sexe',
                key: 'Sex',
                options: {
                    type: 'translation',
                }
            },
            {
                'label':'entity.data_types.header.field_number',
                key: 'number',
                sort: true
            },
            {
                'label':'entity.data_types.header.field_numberLastAdded',
                key: 'numberLastAdded'
            },
            {
                'label':'entity.data_types.header.field_updatedAt',
                key: 'updatedAt',
                options: {
                    type: 'date',
                    format: "DD/MM/YYYY"
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.sex.header.field_sexe',
                key: 'Sex',
                type: 'translation',
                validator: ['required'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                attributes: [],
                option: {
                    key: 'id',
                    value: 'name',
                    templateMultiple: "{{label}} : {{value}}"
                },
                serviceClass: SexesService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/type_cases/%d'
                },
                filter: {
                    type: 'translation',
                    options: {type: 'translation' },
                    service: SexesService,
                    apiRelation: '/api/sexes/%d',
                    fieldDb: 'sexes'
                }
            },{
                label:'entity.data_types.header.field_numberLastAdded',
                key: 'numberLastAdded',
                type: 'text',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['number'],
                option: {
                    min: 0,
                    templateMultiple: "{{label}} : {{value}}"
                }
            },{
                label:'entity.data_types.header.field_number',
                key: 'number',
                type: 'text',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['number'],
                option: {
                    min: 0,
                    templateMultiple: "{{label}} : {{value}}"
                }
            },{
                label:'field.updatedAt',
                key: 'updatedAt',
                type: 'date',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['autoOk', 'fullWidth'],
                option: {
                    format: "dd-MM-yyyy",
                    autoOk: true,
                    fullWidth: true,
                    templateMultiple: "{{label}} : {{value}}"
                }
            },
        ]
    },
};

export default Entity;
