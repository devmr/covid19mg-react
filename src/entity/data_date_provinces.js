import Service from "../service/DataDateProvinceService";
import TypeCaseService from "../service/TypeCaseService";
import ProvinceService from "../service/ProvinceService";


const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_data_date_provinces',
        actions: {
            'pathlist': '/data_date_provinces', // URL to API https://
            'api': '/api/data_date_provinces', // URL to API https://
            'edit': '/edit-data_date_provinces/', // Edit mode
            'editmultiple': '/edit-data_date_provinces-multiple/', // Edit Multiple mode
            'duplicate': true, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-data_date_provinces/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.data_date_provinces.header.field_date',
                key: 'date',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "date",
                },
                sort: true,
                sortkey: 'date.date',
                filter: {
                    type: 'date',
                    fieldDb: 'date.date'
                }
            },
            {
                'label':'entity.data_types.header.field_province',
                key: 'province',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: ProvinceService,
                    fieldDb: 'province'
                }
            },
            {
                'label':'entity.data_types.header.field_typeCase',
                key: 'typeCase',
                options: {
                    type: 'translation',
                },
                filter: {
                    type: 'translation',
                    options: {type: 'translation' },
                    service: TypeCaseService,
                    apiRelation: '/api/type_cases/%d',
                    fieldDb: 'typeCase'
                }
            },
            {
                'label':'entity.data_types.header.field_number',
                key: 'number',
                sort: true,
                filter: {
                    type: 'range',
                    fieldDb: 'number'
                }
            },
            {
                'label':'entity.data_types.header.field_updatedAt',
                key: 'createdAt',
                options: {
                    type: 'date',
                    format: "DD/MM/YYYY"
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'field.createdAt',
                key: 'date',
                type: 'datecreator',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: ['readonly'],
                submit: {
                    format: '/api/dates/%d',
                    plain: false
                },
                option: {
                    format: "DD-MM-YYYY",
                    key: '@id',
                    value: 'date',
                }
            },{
                label:'entity.data_types.header.field_province',
                key: 'province',
                type: 'autocomplete',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: [],
                serviceClass: ProvinceService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/provinces/%d',
                    plain: false
                },
                option: {
                    key: 'id',
                    value: 'name',
                }
            },{
                label:'entity.data_types.header.field_typeCase',
                key: 'typeCase',
                type: 'translation',
                validator: ['required'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                attributes: [],
                option: {
                    key: 'id',
                    value: 'name',
                    templateMultiple: "{{label}} : {{value}}"
                },
                serviceClass: TypeCaseService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/type_cases/%d',
                    plain: true
                },
                filter: {
                    type: 'translation',
                    options: {type: 'translation' },
                    service: TypeCaseService,
                    apiRelation: '/api/type_cases/%d',
                    fieldDb: 'typeCase'
                }


            },{
                label:'entity.data_types.header.field_number',
                key: 'number',
                type: 'text',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['number'],
                option: {
                    min: 0,
                    templateMultiple: "{{label}} : {{value}}"
                }
            }
        ]
    },
};

export default Entity;
