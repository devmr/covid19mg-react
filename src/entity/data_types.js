import Service from "../service/DataTypesService";
import TypeCaseService from "../service/TypeCaseService";


const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_casetypes',
        actions: {
            'pathlist': '/data_types', // URL to API https://
            'api': '/api/data_types', // URL to API https://
            'edit': '/edit-data_types/', // Edit mode
            'editmultiple': '/edit-data_types-multiple/', // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': false, // Delete mode
            'create': false, // Create mode
        },
        headerCell: [
            {
                'label':'entity.data_types.header.field_typeCase',
                key: 'typeCase',
                options: {
                    type: 'translation',
                },
                sort: false
            },
            {
                'label':'entity.data_types.header.field_number',
                key: 'number',
                sort: true,
                filter: {
                    type: 'range',
                    fieldDb: 'number'
                }
            },
            {
                'label':'entity.data_types.header.field_numberLastAdded',
                key: 'numberLastAdded'
            },
            {
                'label':'entity.data_types.header.field_updatedAt',
                key: 'updatedAt',
                options: {
                    type: 'date',
                    format: "DD/MM/YYYY"
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'field.createdAt',
                key: 'createdAt',
                type: 'date',
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: ['readonly'],
                option: {
                    format: "DD-MM-YYYY"
                }
            },{
                label:'entity.data_types.header.field_typeCase',
                key: 'typeCase',
                type: 'translation',
                validator: ['required'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                attributes: [],
                option: {
                    key: 'id',
                    value: 'name',
                    templateMultiple: "{{label}} : {{value}}"
                },
                serviceClass: TypeCaseService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/type_cases/%d',
                    plain: true
                },
                filter: {
                    type: 'translation',
                    options: {type: 'translation' },
                    service: TypeCaseService,
                    apiRelation: '/api/type_cases/%d',
                    fieldDb: 'typeCase'
                }


            },{
                label:'entity.data_types.header.field_numberLastAdded',
                key: 'numberLastAdded',
                type: 'text',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['number'],
                option: {
                    min: 0,
                    templateMultiple: "{{label}} : {{value}}",
                    valueAddedTo: "number" // add value to this field "number"
                }
            },{
                label:'entity.data_types.header.field_number',
                key: 'number',
                type: 'text',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['number'],
                option: {
                    min: 0,
                    templateMultiple: "{{label}} : {{value}}",
                    valueAddedfrom: "numberLastAdded" // add value to this field "numberLastAdded"

                }
            },{
                label:'field.updatedAt',
                key: 'updatedAt',
                type: 'date',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                attributes: ['autoOk', 'fullWidth'],
                option: {
                    format: "dd-MM-yyyy",
                    autoOk: true,
                    fullWidth: true,
                    templateMultiple: "{{label}} : {{value}}"
                }
            },
        ]
    },
};

export default Entity;
