import Service from "../service/QuartiersService";
import CommunesService from "../service/CommunesService";
import DistrictService from "../service/DistrictService";
import RegionService from "../service/RegionService";
import ProvinceService from "../service/ProvinceService";

const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_quartiers',
        actions: {
            'pathlist': '/quartiers', // URL to API https://
            'api': '/api/quartiers', // URL to API https://
            'edit': '/edit-quartiers/', // Edit mode
            'editmultiple': false, // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-quartiers/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.quartiers.header.field_name',
                key: 'name',
                sort: true,
                filter: {
                    type: 'text',
                    fieldDb: 'name'
                }
            },
            {
                'label':'entity.quartiers.header.field_province',
                key: 'province',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: ProvinceService,
                    fieldDb: 'province'
                }
            },
            {
                'label':'entity.quartiers.header.field_region',
                key: 'region',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: RegionService,
                    fieldDb: 'region'
                }
            },
            {
                'label':'entity.quartiers.header.field_district',
                key: 'district',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: DistrictService,
                    fieldDb: 'district'
                }
            },
            {
                'label':'entity.quartiers.header.field_commune',
                key: 'commune',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: CommunesService,
                    fieldDb: 'commune'
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.quartiers.header.field_name',
                key: 'name',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.quartiers.header.field_commune',
                key: 'commune',
                type: 'autocomplete',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: [],
                serviceClass: CommunesService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/communes/%d',
                    plain: false
                },
                option: {
                    key: 'id',
                    value: 'name',
                }
            }
        ]
    },
};

export default Entity;
