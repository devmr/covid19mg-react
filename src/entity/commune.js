import Service from "../service/CommunesService";
import DistrictService from "../service/DistrictService";
import RegionService from "../service/RegionService";
import ProvinceService from "../service/ProvinceService";

const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_communes',
        actions: {
            'pathlist': '/communes', // URL to API https://
            'api': '/api/communes', // URL to API https://
            'edit': '/edit-communes/', // Edit mode
            'editmultiple': false, // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-communes/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.communes.header.field_name',
                key: 'name',
                sort: true,
                filter: {
                    type: 'text',
                    fieldDb: 'name'
                }
            },
            {
                'label':'entity.communes.header.field_province',
                key: 'province',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: ProvinceService,
                    fieldDb: 'province'
                }
            },
            {
                'label':'entity.communes.header.field_region',
                key: 'region',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: RegionService,
                    fieldDb: 'region'
                }
            },
            {
                'label':'entity.communes.header.field_district',
                key: 'district',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: DistrictService,
                    fieldDb: 'district'
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.communes.header.field_name',
                key: 'name',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.communes.header.field_district',
                key: 'district',
                type: 'autocomplete',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: [],
                serviceClass: DistrictService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/districts/%d',
                    plain: false
                },
                option: {
                    key: 'id',
                    value: 'name',
                }
            }
        ]
    },
};

export default Entity;
