import Service from "../service/TranslationService";
import LocaleService from "../service/LocaleService";
import TranslationTypesService from "../service/TranslationTypesService";


const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_translations',
        actions: {
            'pathlist': '/translations', // URL to API https://
            'api': '/api/translations', // URL to API https://
            'edit': '/edit-translations/', // Edit mode
            'editmultiple': '/edit-translations-multiple/', // Edit Multiple mode
            'duplicate': true, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-translations/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.translation.header.field_translation',
                key: 'translation',
                options: {
                    type: 'string',
                    // format: 'replace',
                    // strSearch: [
                    //     '/api/type_cases/1',
                    //     '/api/type_cases/2',
                    //     '/api/type_cases/3',
                    // ],
                    // strReplace: [
                    //     'Cas Confirmés',
                    //     'Guéris',
                    //     'Décès',
                    // ],
                },
                sort: true,
                filter: {
                    type: 'text',
                    fieldDb: 'translation'
                }
            },
            {
                'label':'entity.translation.header.field_keytranslate',
                key: 'keytranslate',
                filter: {
                    type: 'text',
                    fieldDb: 'keytranslate'
                }
            },
            {
                'label':'entity.translation.header.field_locale',
                key: 'locale',
                filter: {
                    type: 'relation',
                    options: {type: 'translation' },
                    service: LocaleService,
                    fieldDb: 'locale'
                }
            },
            {
                'label':'entity.translation.header.field_domain',
                key: 'domain'
            },
            {
                hidden: true,
                'label':'entity.translation.header.field_translationType',
                key: 'translationType',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'nativeEntity',
                    service: TranslationTypesService,
                    apiRelation: '/api/translation_types/%d',
                    fieldDb: 'translationType'
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.translation.header.field_keytranslate',
                key: 'keytranslate',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                submit: {
                    patternSearch: /[^a-z0-9_.@]/gi,
                    patternReplace: '_'
                },
                // Handlebars
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.translation.header.field_translation',
                key: 'translation',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                attributes: ['multiline'],
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.translation.header.field_locale',
                key: 'locale',
                type: 'relation',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                serviceClass: Service,
                serviceMethod: 'getAllLang',
                // Handlebars
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.translation.header.field_domain',
                key: 'domain',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: true,
                defaultValue: 'messages',
                // Handlebars
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
                submit: {
                    joinfield: 'intlicu',
                    valuefield: '+intl-icu'
                },
            },{
                label:'+intl-icu',
                key: 'intlicu',
                type: 'switch',
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                inversedfield: 'domain'
            },{
                label:'entity.translation.header.field_translationType',
                key: 'translationType',
                type: 'nativeEntity',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                serviceClass: TranslationTypesService,
                serviceMethod: 'findAll',
                option: {
                    key: 'id',
                    value: 'name'
                },
                submit: {
                    format: '/api/translation_types/%d',
                    plain: true
                }

            }
        ]
    },
};

export default Entity;
