import Service from "../service/DistrictService";
import RegionService from "../service/RegionService";
import ProvinceService from "../service/ProvinceService";

const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_districts',
        actions: {
            'pathlist': '/districts', // URL to API https://
            'api': '/api/districts', // URL to API https://
            'edit': '/edit-districts/', // Edit mode
            'editmultiple': false, // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-districts/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.district.header.field_name',
                key: 'name',
                sort: true,
                filter: {
                    type: 'text',
                    fieldDb: 'name'
                }
            },
            {
                'label':'entity.district.header.field_province',
                key: 'province',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: ProvinceService,
                    fieldDb: 'province'
                }
            },
            {
                'label':'entity.district.header.field_region',
                key: 'region',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: RegionService,
                    fieldDb: 'region'
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.district.header.field_name',
                key: 'name',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.district.header.field_region',
                key: 'region',
                type: 'autocomplete',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: [],
                serviceClass: RegionService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/regions/%d',
                    plain: false
                },
                option: {
                    key: 'id',
                    value: 'name',
                }
            }
        ]
    },
};

export default Entity;
