import Service from "../service/RegionService";
import ProvinceService from "../service/ProvinceService";

const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_regions',
        actions: {
            'pathlist': '/regions', // URL to API https://
            'api': '/api/regions', // URL to API https://
            'edit': '/edit-regions/', // Edit mode
            'editmultiple': false, // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-regions/', // Create mode
        },
        headerCell: [
            {
                'label':'entity.region.header.field_name',
                key: 'name',
                sort: true,
                filter: {
                    type: 'text',
                    fieldDb: 'name'
                }
            },
            {
                'label':'entity.region.header.field_province',
                key: 'province',
                options: {
                    type: 'relation',
                    template: "%s",
                    fields: "name"
                },
                filter: {
                    type: 'autocomplete',
                    service: ProvinceService,
                    fieldDb: 'province'
                }
            }
        ]
    },
    form: {
        fields: [
            {
                label:'entity.region.header.field_name',
                key: 'name',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },{
                label:'entity.region.header.field_province',
                key: 'province',
                type: 'autocomplete',
                validator: ['required'],
                insideRowMultiplePrimary: false,
                insideRowMultipleSecondary: false,
                attributes: [],
                serviceClass: ProvinceService,
                serviceMethod: 'findAll',
                submit: {
                    format: '/api/provinces/%d',
                    plain: false
                },
                option: {
                    key: 'id',
                    value: 'name',
                }
            }
        ]
    },
};

export default Entity;
