import Service from "../service/ProvinceService";

const Entity =  {
    service: Service,
    list: {
        titlePage: 'text.title_provinces',
        actions: {
            'pathlist': '/provinces', // URL to API https://
            'api': '/api/provinces', // URL to API https://
            'edit': '/edit-provinces/', // Edit mode
            'editmultiple': '/edit-provinces-multiple/', // Edit Multiple mode
            'duplicate': false, // Copy mode
            'delete': true, // Delete mode
            'create': '/create-provinces/', // Create mode
        },
        headerCell: [
            {
                label:'entity.province.header.field_name',
                key: 'name',
                sort: true
            },
        ]
    },
    form: {
        fields: [
            {
                label:'entity.province.header.field_name',
                key: 'name',
                type: 'text',
                validator: ['required', 'string'],
                insideRowMultiplePrimary: true,
                insideRowMultipleSecondary: false,
                option: {
                    templateMultiple: "{{label}} : {{value}}"
                },
            },
        ]
    },
};

export default Entity;
