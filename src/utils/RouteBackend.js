import React from 'react';

// react-router
import {
    Route,
    Switch
} from 'react-router-dom'

// Dashboard
import Index from "../views/home/";

// Provinces
import Provinces from "../views/province/";
import EditProvinces from "../views/province/edit";
import EditMultipleProvinces from "../views/province/edit_multiple";
import CreateProvinces from "../views/province/create";

// regions
import Region from "../views/region/";
import EditRegion from "../views/region/edit";
import CreateRegion from "../views/region/create";

//Districts
import Districts from "../views/district/";
import EditDistricts from "../views/district/edit";
import CreateDistricts from "../views/district/create";

// Communes
import Communes from "../views/commune/";
import EditCommunes from "../views/commune/edit";
import CreateCommunes from "../views/commune/create";

// Quartiers
import Quartiers from "../views/quartiers/";
import EditQuartiers from "../views/quartiers/edit";
import CreateQuartiers from "../views/quartiers/create";

// DataTypes
import DataTypes from "../views/data_types/";
import EditDataTypes from "../views/data_types/edit";
import EditMultipleDataTypes from "../views/data_types/edit_multiple";

// DataTypes provinces
import DataProvinces from "../views/data_provinces/";
import EditDataProvinces from "../views/data_provinces/edit";
import EditMultipleDataProvinces from "../views/data_provinces/edit_multiple";
import CreateDataProvinces from "../views/data_provinces/create";

// DataTypes regions
import DataRegions from "../views/data_region/";
import EditDataRegions from "../views/data_region/edit";
import EditMultipleDataRegions from "../views/data_region/edit_multiple";
import CreateDataRegions from "../views/data_region/create";

// DataTypes districts
import DataDistricts from "../views/data_district/";
import EditDataDistricts from "../views/data_district/edit";
import EditMultipleDataDistricts from "../views/data_district/edit_multiple";
import CreateDataDistricts from "../views/data_district/create";

// DataTypes sexes
import DataSexes from "../views/data_sexes/";
import EditDataSexes from "../views/data_sexes/edit";
import CreateDataSexes from "../views/data_sexes/create";

// DataTypes Date
import DataDateTypes from "../views/data_date_types/";
import EditDataDateTypes from "../views/data_date_types/edit";
import EditMultipleDataDateTypes from "../views/data_date_types/edit_multiple";
import CreateDataDateTypes from "../views/data_date_types/create";

// DataTypes Date Province
import DataDateProvinces from "../views/data_date_province/";
import EditDataDateProvinces from "../views/data_date_province/edit";
import EditMultipleDataDateProvinces from "../views/data_date_province/edit_multiple";
import CreateDataDateProvinces from "../views/data_date_province/create";

// DataTypes Date Regions
import DataDateRegions from "../views/data_date_region/";
import EditDataDateRegions from "../views/data_date_region/edit";
import EditMultipleDataDateRegions from "../views/data_date_region/edit_multiple";
import CreateDataDateRegions from "../views/data_date_region/create";

// DataTypes Date Districts
import DataDateDistricts from "../views/data_date_district/";
import EditDataDateDistricts from "../views/data_date_district/edit";
import EditMultipleDataDateDistricts from "../views/data_date_district/edit_multiple";
import CreateDataDateDistricts from "../views/data_date_district/create";

// Settings
import Settings from "../views/setting/";

// Translation
import Translation from "../views/translation/";
import EditTranslation from "../views/translation/edit";
import EditMultipleTranslation from "../views/translation/edit_multiple";
import CreateTranslation from "../views/translation/create";

const RouteBackend = () => {
    return (
        <Switch>
            <Route path="/home" component={Index} />
            {/* --------------- */}
            {/* Places */}
            {/* --------------- */}
            {/* Places - Province */}
            <Route path='/provinces' component={Provinces} />
            <Route path='/edit-provinces/:id' component={EditProvinces} />
            <Route path='/edit-provinces-multiple/:id' component={EditMultipleProvinces} />
            <Route path='/create-provinces/' component={CreateProvinces} />
            {/* Places - Regions */}
            <Route path='/regions' component={Region} />
            <Route path='/edit-regions/:id' component={EditRegion} />
            <Route path='/create-regions/' component={CreateRegion} />
            {/* Places - Districts */}
            <Route path='/districts' component={Districts} />
            <Route path='/edit-districts/:id' component={EditDistricts} />
            <Route path='/create-districts/' component={CreateDistricts} />
            {/* Places - Communes */}
            <Route path='/communes' component={Communes} />
            <Route path='/edit-communes/:id' component={EditCommunes} />
            <Route path='/create-communes/' component={CreateCommunes} />
            {/* Places - Quartiers */}
            <Route path='/quartiers' component={Quartiers} />
            <Route path='/edit-quartiers/:id' component={EditQuartiers} />
            <Route path='/create-quartiers/' component={CreateQuartiers} />
            {/* --------------- */}
            {/* Datas */}
            {/* --------------- */}
            {/* Data - global */}
            <Route  path='/data_types' component={DataTypes} />
            <Route  path='/edit-data_types/:id' component={EditDataTypes} />
            <Route  path='/edit-data_types-multiple/:id' component={EditMultipleDataTypes} />
            {/* Data - provinces */}
            <Route  path='/data_provinces' component={DataProvinces} />
            <Route  path='/edit-data_provinces/:id'  component={EditDataProvinces} />
            <Route  path='/edit-data_provinces-multiple/:id'  component={EditMultipleDataProvinces} />
            <Route  path='/create-data_provinces/'  component={CreateDataProvinces} />
            {/* Data - regions */}
            <Route  path='/data_regions' component={DataRegions} />
            <Route  path='/edit-data_regions/:id'  component={EditDataRegions} />
            <Route  path='/edit-data_regions-multiple/:id'  component={EditMultipleDataRegions} />
            <Route  path='/create-data_regions/'  component={CreateDataRegions} />
            {/* Data - districts */}
            <Route  path='/data_districts' component={DataDistricts} />
            <Route  path='/edit-data_districts/:id'  component={EditDataDistricts} />
            <Route  path='/edit-data_districts-multiple/:id'  component={EditMultipleDataDistricts} />
            <Route  path='/create-data_districts/'  component={CreateDataDistricts} />
            {/* Data - sex */}
            <Route  path='/data_sexes' component={DataSexes} />
            <Route  path='/edit-data_sexes/:id'  component={EditDataSexes} />
            <Route  path='/create-data_sexes/'  component={CreateDataSexes} />
            {/* Data - Dates */}
            <Route  path='/data_date_types' component={DataDateTypes} />
            <Route  path='/edit-data_date_types/:id'  component={EditDataDateTypes} />
            <Route path='/edit-data_date_types-multiple/:id' component={EditMultipleDataDateTypes} />
            <Route  path='/create-data_date_types/'  component={CreateDataDateTypes} />
             {/* Data - Dates provinces */}
             <Route  path='/data_date_provinces' component={DataDateProvinces} />
             <Route  path='/edit-data_date_provinces/:id'  component={EditDataDateProvinces} />
             <Route path='/edit-data_date_provinces-multiple/:id' component={EditMultipleDataDateProvinces} />
             <Route  path='/create-data_date_provinces/'  component={CreateDataDateProvinces} />
            {/* Data - Dates regions */}
            <Route  path='/data_date_regions' component={DataDateRegions} />
            <Route  path='/edit-data_date_regions/:id'  component={EditDataDateRegions} />
            <Route path='/edit-data_date_regions-multiple/:id' component={EditMultipleDataDateRegions} />
            <Route  path='/create-data_date_regions/'  component={CreateDataDateRegions} />
            {/* Data - Dates districts */}
            <Route  path='/data_date_districts' component={DataDateDistricts} />
            <Route  path='/edit-data_date_districts/:id'  component={EditDataDateDistricts} />
            <Route path='/edit-data_date_districts-multiple/:id' component={EditMultipleDataDateDistricts} />
            <Route  path='/create-data_date_districts/'  component={CreateDataDateDistricts} />
            {/* --------------- */}
            {/* Others */}
            {/* --------------- */}
            {/* Other - Translations */}
            <Route path='/translations' component={Translation} />
            <Route path='/edit-translations/:id' component={EditTranslation} />
            <Route path='/edit-translations-multiple/:id' component={EditMultipleTranslation} />
            <Route path='/create-translations' component={CreateTranslation} />
            {/* Other - Settings */}
            <Route path='/settings' component={Settings} />
        </Switch>
    )
}

export default RouteBackend;
