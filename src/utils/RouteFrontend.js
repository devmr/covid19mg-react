import React from 'react';

// react-router
import {
    Route
} from 'react-router-dom'

// Login
import Login from "../views/user/Login";

const RouteFrontend = () => {
    return (
        <React.Fragment>
            <Route path="/" component={Login} />
        </React.Fragment>
    )
}

export default RouteFrontend;
