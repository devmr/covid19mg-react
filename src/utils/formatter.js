
import React from "react";
import Moment from 'react-moment';
import strings from "locutus/php/strings"

export const Formatter = {

    format: function (value,type) {

        if (typeof type === 'undefined' || typeof type.options === 'undefined') {
            return value;
        }

        let typeField = typeof type.options.type === 'undefined' ? 'string' : type.options.type;

        switch (typeField) {
            case 'array':
                return Formatter.formatArrayCount(value, type.options);

            case 'date':
                return Formatter.formatDate(value, type.options);

            case 'relation':
                return Formatter.formatLink(value, type.options);

            case 'translation':
                return Formatter.formatTranslation(value, type.options);

            default:
            case 'string':
                return Formatter.formatText(value, type.options);
        }
    },

    formatArrayCount(value, option) {
        if (typeof option.format === 'undefined') {
            option.format = "count"
        }

        if (option.format === 'count') {
            return typeof value !== 'undefined' ? value.length : 0;
        }

        return typeof value !== 'undefined' ? value : null;
    },

    formatText(value, option) {
        if (typeof option.format === 'undefined') {
            option.format = ""
        }

        if (option.format === 'replace' && typeof option.strSearch !== 'undefined' && typeof option.strReplace !== 'undefined') {
            let i = 0;
            option.strSearch.forEach(strFind => {
                let strReplaced = typeof option.strReplace[i] !== 'undefined' ? option.strReplace[i] : strFind;
                value = value.replace(strFind, strReplaced);
                i++;
            })

        }

        return value;
    },

    formatLink(value, option)  {
        if (typeof option.format === 'undefined') {
            option.format = ""
        }

        let args = [];

        option.fields.split(',').map( (row, index) => {
            if (value !== null && value[row] !== null) {
                let valueField = value[row];
                // console.log(option.fields_options[index])
                // Formatter.format(valueField)
                args.push(valueField);
            }
            return args;
        });

        return (
            <React.Fragment>
                {strings.vsprintf(option.template, args)}
            </React.Fragment>
        )
    },

    formatTranslation(value, option)  {
        if (typeof option.format === 'undefined') {
            option.format = ""
        }

        if (typeof value === 'undefined' || typeof value['TranslationDb'] === 'undefined') {
            return '';
        }

        let args = [];
        value['TranslationDb'].map( (row) => {
            if (typeof row['translation'] !== 'undefined' && typeof row['locale'] !== 'undefined') {
                args.push(strings.sprintf('%s (%s)', row['translation'], row['locale']));
            }
            return args;
        });

        return (
            <React.Fragment>
                {args.join(' / ')}
            </React.Fragment>
        )
    },


    formatDate(value, option) {

        if (typeof option.format === 'undefined') {
            option.format = "DD/MM"
        }

        return (
            <React.Fragment>
                <Moment format={option.format}>{value}</Moment>
            </React.Fragment>
        )
    }
}
