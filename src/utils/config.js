const baseUrl = 'https://devcovid19mg.madatsara.com'

export const Config = {

    host: function () {
        return typeof process.env.REACT_APP_API_URL !== 'undefined' ? process.env.REACT_APP_API_URL : baseUrl;
    }

    ,getFilterOptionsRange: function () {
        return {
            'lt': 'field.range.lt',
            'lte': 'field.range.lte',
            'gt': 'field.range.gt',
            'gte': 'field.range.gte'
        }
    }

    ,getFilterOptionsDate: function () {
        return {
            'strictly_before': 'field.range_date.strictly_before',
            'before': 'field.range_date.before',
            'after': 'field.range_date.after',
            'strictly_after': 'field.range_date.strictly_after',
            // 'between': 'field.range.between_dates'
        }
    }

    ,getFieldstypeWithRange: function () {
        return ['date', 'range'];
    },

    getMappingApiAutocompletefield: function () {
        return {
            'autocomplete_district': '/api/districts/%d',
            'autocomplete_province': '/api/provinces/%d',
            'autocomplete_region': '/api/regions/%d',
            'autocomplete_commune': '/api/communes/%d'
        }
    }

    ,isEmpty: function (s) {

        if (typeof s === 'number' && isNaN(s)) {
            return true;
        }

        if (typeof s === 'string' && s.trim() === '') {
            return true;
        }

        return false;

    }
}
