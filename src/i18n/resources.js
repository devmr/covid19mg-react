const resources = {
    mg: {
        translation: {
            "text": {
                "loading": "Misoko miadana...",
                "success_loggedin" : "Milay - afaka segondra vitsy dia tafiditra",
                "success_updated" : "Tafita soa aman-tsara ny fanovana",
                "success_removed" : "Tafita soa aman-tsara ny famafana",
                "success_removed_plural" : "Tafita soa aman-tsara ny famafana : {{count}}",
                "title" : "COVID19mg - Fandrindrana ny kirakira",
                "dialog_logout_title": "Mivoaka",
                "dialog_remove_title": "Fafana",
                "dialog_confirm_ask": "Ekenao ve ny tranga ateraky ny traika ?",
                "title_dashboard": "Fandraisana",
                "title_casetypes": "Antontan'isa",
                "title_create": "Manorina",
                "title_edit": "Manova",
                "title_provinces": "Faritany",
                "title_regions": "Faritra",
                "title_districts": "Distrika",
                "title_settings": "Lamina",
                "title_translations": "Dika teny",
                "entityselected": "safidy: {{count}}",
                "entityselected_plural": "safidy: {{count}}",
                "title_data_province": "Antontan'isa faritra",
                "title_data_sexe": "Antontan'isa lahy/vavy",
            },
            "tooltip": {
                "show_password" : "Aseho ny tenimiafina",
                "hide_password" : "Afenina ny tenimiafina",
                "logout" : "Mivoaka",
                "create": "Ampiana",
                "copy": "Adika",
                "edit_multiple": "Ovaina maro",
                "edit": "Ovaina",
                "delete": "Fafana",
                "back": "Miverina amin'ny lisitra",
            },
            "error": {
                "field_empty": "Mila fenoina",
                "email_incorrect": "Email diso tsipelina",
                "field_incorrect": "Diso tsipelina"
            },
            "field": {
                "username": "Tenifidirana",
                "password": "Tenimiafina",
                "button_login": "Miditra",
                "button_logout": "Mivoaka",
                "button_yes": "Eny",
                "button_no": "Tsia",
                "button_switch_lang": "Chg. langue",
                "button_save": "Raisina",
                "updatedAt": "Daty (fandraisana farany)",
                "createdAt": "Daty (fanorenana)"
            },
            "sidebar_menu": {
                "dashboard" : "Fandraisana",
                "casetypes" : "Antontan'isa",
                "provinces" : "Faritany",
                "regions" : "Faritra",
                "districts" : "Distrika",
                "translations" : "Dika teny",
                "settings" : "Lamina",
                "extra" : "Hafa",
                "place" : "Toerana",
                "casetype_sexe" : "par sexe",
                "casetype_date" : "par date",
                "casetype_place" : "par province",
            },
            "menu": {
                "create": "Ampiana",
                "copy": "Adika",
                "edit_multiple": "Ovaina maro",
                "edit": "Ovaina",
                "delete": "Fafana",
            },
            "entity" :{
                "casetype" : {
                    "header": {
                        "field_typeCase" : "Tranga",
                        "field_number" : "Isa rehetra",
                        "field_numberLastAdded" : "Isa (tafiditra farany)",
                        "field_updatedAt" : "Daty (tafiditra farany)",
                    }
                },
                "province" : {
                    "header": {
                        "field_name" : "Anarana",
                    }
                },
                "region" : {
                    "header": {
                        "field_name" : "Anarana",
                        "field_province" : "Rohy (Faritany)",
                        "field_district_count" : "Isa (Distrika)",
                    }
                },
                "district" : {
                    "header": {
                        "field_name" : "Anarana",
                        "field_province" : "Rohy (Faritany)",
                        "field_region" : "Rohy (Faritra)",
                        "field_commune_count" : "Isa (Kaominina)",
                    }
                },
                "translation" : {
                    "header": {
                        "field_translation" : "Teny voadika",
                        "field_keytranslate" : "Fampahafantarana",
                        "field_locale" : "Fiteny",
                        "field_domain" : "Vondrona",
                    }
                },
                "data_date_type" : {
                    "header": {
                        "field_date" : "Date",
                    }
                },
            }
        }
    },
    fr: {
        translation: {
            "text": {
                "loading": "Chargement...",
                "success_loggedin" : "Réussie - Redirection dans quelques secondes",
                "success_updated" : "Mise à jour réussie",
                "success_removed" : "Suppression réussie",
                "success_created" : "Création réussie",
                "success_removed_plural" : "{{count}} lignes supprimées",
                "success_duplicated" : "Copie réussie - aller au dernier enregistrement/dernière page pour voir la ligne copiée",
                "success_duplicated_plural" : "{{count}} lignes copiées - aller au dernier enregistrement/dernière page pour voir les lignes copiées",
                "title" : "COVID19mg - App",
                "dialog_logout_title": "Déconnexion",
                "dialog_filter_title": "Filtre / recherche",
                "dialog_remove_title": "Supprimer",
                "dialog_confirm_title": "Demande de confirmation",
                "dialog_add_date": "Choisir une date ",
                "dialog_confirm_ask": "Êtes vous sur de cette action ?",
                "dialog_confirm_savemultiple": "Êtes vous sur vouloir mettre à jour tous les enregistrements avec les données dans ce formulaire ?",
                "title_dashboard": "Dashboard",
                "title_casetypes": "Données globales",
                "title_create": "Créer",
                "title_edit": "Modifier",
                "title_edit_multiple": "Modifier plusieurs enregistrements",
                "title_provinces": "Provinces",
                "title_regions": "Régions",
                "title_districts": "Districts",
                "title_communes": "Communes",
                "title_quartiers": "Quartiers",
                "title_settings": "Paramètres",
                "title_translations": "Traductions",
                "entityselected": "{{count}} coché",
                "entityselected_plural": "{{count}} cochés",
                "title_data_province": "Données par province",
                "title_data_regions": "Données par régions",
                "title_data_districts": "Données par districts",
                "title_data_sexe": "Données par sexe",
                "title_data_date_type": "Données par dates / types",
                "title_data_date_provinces": "Données par dates / types / provinces",
                "title_data_date_regions": "Données par  dates / types / régions",
                "title_data_date_districts": "Données par  dates / types / districts",
                "loading_task": "Tâche en cours d'execution...",
                "success_date_found" : "Date trouvée correctement.",
                "success_date_found" : "Date créée correctement.",
                "success_duplicated_multiple" : "Enregistrements copiées correctement.",
                "success_updated_multiple" : "Enregistrements mises à jour correctement. Redirection automatique dans quelques secondes vers la page liste ...",
            },
            "tooltip": {
                "show_password" : "Afficher le mot de passe",
                "hide_password" : "Cacher le mot de passe",
                "logout" : "Déconnexion",
                "create": "Créer",
                "copy": "Copier",
                "edit_multiple": "Modification multiple",
                "edit": "Modifier",
                "delete": "Supprimer",
                "back": "Retour à la liste",
                "remove_from_list": "Supprimer de la liste",
                "updated_from_number_added": "Valeur mise à jour et additionnée de la valeur dans ajout (du jour)",
                "value_from_db": "Valeur enregistrée avant modification",
                "filter": "Chercher/Filtrer par",

            },
            "error": {
                "field_empty": "Champ obligatoire",
                "email_incorrect": "Adresse email incorrecte",
                "field_incorrect": "Format incorrect",
            },
            "field": {
                "username" : "Identifiant",
                "password" : "Mot de passe",
                "button_login" : "Connexion",
                "button_logout" : "Déconnexion",
                "button_yes" : "Oui",
                "button_no" : "Non",
                "button_search_filter" : "Chercher/Filtrer",
                "button_search_reset_filter" : "Annuler le filtre",
                "button_close_dialog" : "Fermer sans chercher",
                "button_cancel" : "Annuler",
                "button_switch_lang" : "Ovaina fiteny",
                "button_save" : "Enregistrer",
                "button_save_update" : "Mettre à jour",
                "button_save_update_all" : "Tout mettre à jour",
                "button_createnew_all" : "Cloner tout avec les enregistrements",
                "updatedAt": "Date (mise à jour)",
                "createdAt": "Date (création)",
                "button_showmore": "Afficher plus",
                "button_showmore_plural": "Afficher les {{count}} enregistrements",
                "button_showless": "Afficher moins",
                "button_choose_date": "Choisir une date",
                "button_choose_other_date": "Choisir une autre date",
                "select_none": "-",
                "button_search_date" : "Choisir cette date",
                "remember_me" : "Se souvenir de moi",
                "range": {
                    "lt": "Inférieur à ",
                    "lte": "Inférieur ou égal à ",
                    "gt": "Supérieur à ",
                    "gte": "Supérieur ou égal à ",
                    "eq": "Egal à ",
                    "between": "Entre ",
                    "between_and": " et ",
                    "between_dates": "Entre 2 dates "
                },
                "range_date": {
                    "strictly_before": "Avant le ",
                    "before": "Avant le (y compris)",
                    "after": "Après le (y compris)",
                    "strictly_after": "Après le "
                },
                "search": {
                    "ipartial": "Contient",
                    "start": "Commence par ",
                    "end": "Finit par ",
                    "eq": "Egal à "
                }
            },
            "sidebar_menu": {
                "dashboard" : "Dashboard",
                "data" : "Données",
                "data_types" : "Données globales",
                "provinces" : "Provinces",
                "regions" : "Régions",
                "districts" : "Districts",
                "communes" : "Communes",
                "quartiers" : "Quartiers",
                "translations" : "Traductions",
                "settings" : "Paramètres",
                "extra" : "Extra",
                "place" : "Entités lieux",
                "data_sexes" : "par date",
                "data_sexes" : "par sexe",
                "data_provinces" : "par province",
                "data_regions" : "par region",
                "data_districts" : "par district",
                "data_date_types" : "Données par dates/Types",
                "data_date_provinces" : "par province",
                "data_date_regions" : "par région",
                "data_date_districts" : "par district",
            },
            "menu": {
                "create": "Créer",
                "copy": "Copier",
                "edit_multiple": "Modification multiple",
                "edit": "Modifier",
                "delete": "Supprimer",
            },
            "entity" :{
                "data_types" : {
                    "header": {
                        "field_typeCase" : "Cas",
                        "field_number" : "Nombre total",
                        "field_numberLastAdded" : "Dernier nombre ajouté",
                        "field_updatedAt" : "Date (mise à jour)",
                        "field_createdAt" : "Date (création)",
                        "field_province" : "Province",
                        "field_regions" : "Région",
                        "field_districts" : "District",
                    },
                    "field_typeCase" : "Cas"
                },
                "province" : {
                    "header": {
                        "field_name" : "Nom",
                    }
                },
                "region" : {
                    "header": {
                        "field_name" : "Nom",
                        "field_province" : "Province",
                        "field_district_count" : "Nombre (District)",
                    }
                },
                "district" : {
                    "header": {
                        "field_name" : "Nom",
                        "field_province" : "Province",
                        "field_region" : "Région",
                        "field_commune_count" : "Nombre (Commune)",
                    }
                },
                "communes" : {
                    "header": {
                        "field_name" : "Nom",
                        "field_province" : "Province",
                        "field_region" : "Région",
                        "field_district" : "Districts",
                        "field_quartier_count" : "Nombre (Quartiers)",
                    }
                },
                "quartiers" : {
                    "header": {
                        "field_name" : "Nom",
                        "field_province" : "Province",
                        "field_region" : "Région",
                        "field_district" : "Districts",
                        "field_commune" : "Commune"
                    }
                },
                "translation" : {
                    "header": {
                        "field_translation" : "Mot traduit",
                        "field_to_translate" : "Mot à traduire",
                        "field_keytranslate" : "Clé de traduction",
                        "field_locale" : "Langue",
                        "field_domain" : "Domaine",
                        "field_translationType" : "Type",
                    }
                },
                "sex" : {
                    "header": {
                        "field_sexe" : "Sexe",
                    }
                },
                "data_date_type" : {
                    "header": {
                        "field_date" : "Date",
                    }
                },
                "data_date_provinces" : {
                    "header": {
                        "field_date" : "Date",
                    }
                },

            }
        }
    },
}

export default resources;
